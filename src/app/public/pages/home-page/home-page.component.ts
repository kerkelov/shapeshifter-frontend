import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
    fragmentChangeSubscription: Subscription;

    constructor(private route: ActivatedRoute) {}

    ngOnInit(): void {
        this.fragmentChangeSubscription = this.route.fragment.subscribe(
            this.scrollTo
        );
    }

    ngOnDestroy(): void {
        this.fragmentChangeSubscription.unsubscribe();
    }

    scrollTo(id: string | null): void {
        if (id) {
            document.getElementById(id)?.scrollIntoView({ behavior: 'smooth' });
        }
    }
}
