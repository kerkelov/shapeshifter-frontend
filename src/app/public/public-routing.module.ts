import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsNotLoggedInGuard } from '../core/guards/is-not-logged-in.guard';
import { AccessDeniedPageComponent } from '../shared/pages/access-denied-page/access-denied-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    { path: 'home', component: HomePageComponent },
    {
        path: 'login',
        component: LoginPageComponent,
        canActivate: [IsNotLoggedInGuard]
    },
    {
        path: 'register',
        component: RegisterPageComponent,
        canActivate: [IsNotLoggedInGuard]
    },
    { path: 'access-denied', component: AccessDeniedPageComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PublicRoutingModule {}
