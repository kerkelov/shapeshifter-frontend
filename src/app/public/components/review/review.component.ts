import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
    @Input()
    imageSource: string;

    @Input()
    name: string;

    @Input()
    starsCount: number;

    @Input()
    shouldAddHalfStarAtTheEnd: boolean = false;

    emptyArrayOfSizeStarsCount: number[];

    ngOnInit(): void {
        if(!Array.isArray(this.starsCount)){
            this.emptyArrayOfSizeStarsCount = Array(this.starsCount);
        }
    }
}