import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent {
    @Input()
    title: string;

    @Input()
    imageSource: string;

    @Input()
    blogSource: string;
}