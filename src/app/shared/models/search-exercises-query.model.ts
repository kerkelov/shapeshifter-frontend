export type SearchExercisesQuery = {
    primaryMuscleGroups?: string[];
    secondaryMuscleGroups?: string[];
    flag?: 'and' | 'or';
};
