import { Exercise } from '../../main/exercises/models/exercise.model';
import { SearchExercisesQuery } from './search-exercises-query.model';

export type ExercisesSearch = {
    results: Exercise[];
    searchedQuery: SearchExercisesQuery;
};
