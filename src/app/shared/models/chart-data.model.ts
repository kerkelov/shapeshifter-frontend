export type Serie = { value: number; name: string };

export type ChartData = {
    name: string;
    series: Serie[];
};
