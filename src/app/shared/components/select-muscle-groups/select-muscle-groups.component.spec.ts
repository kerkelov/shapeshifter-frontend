import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMuscleGroupsComponent } from './select-muscle-groups.component';

describe('SelectMuscleGroupsComponent', () => {
  let component: SelectMuscleGroupsComponent;
  let fixture: ComponentFixture<SelectMuscleGroupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectMuscleGroupsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMuscleGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
