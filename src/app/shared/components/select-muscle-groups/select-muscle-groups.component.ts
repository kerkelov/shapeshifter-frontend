import {
    Component,
    ElementRef,
    Input,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import {
    map,
    merge,
    Observable,
    of,
    startWith,
    Subject,
    Subscription
} from 'rxjs';
import { allMuscleGroups } from '../../../main/exercises/models/all-muscle-groups';

@Component({
    selector: 'app-select-muscle-groups',
    templateUrl: './select-muscle-groups.component.html',
    styleUrls: ['./select-muscle-groups.component.scss']
})
export class SelectMuscleGroupsComponent implements OnInit, OnDestroy {
    @Input() label: string = 'Select muscle...';
    @Input() allowOnlyOneSelection: boolean = false;
    @Input() controlToKeepUpdated: AbstractControl | null;

    musclesControl: FormControl = new FormControl('');
    filteredMuscles: Observable<string[]>;
    forceAutocompleteOptionsFiltration: Subject<string> = new Subject();
    selectedMuscles: string[];
    notSelectedMuscles: string[];
    allSubscriptions: Subscription[] = [];

    @ViewChild('musclesInput') musclesInput: ElementRef<HTMLInputElement>;

    get canAddMore(): boolean {
        return !(
            (this.allowOnlyOneSelection && this.selectedMuscles.length === 1) ||
            this.selectedMuscles.length === allMuscleGroups.length
        );
    }

    ngOnInit(): void {
        this.changeTheOptionsInTheAutocompleteDependingOnWhatIsTyped();
        this.updateTheValueOfSelectedMuscleGroupsWhenTheControlToKeepUpdatedChanges();
    }

    ngOnDestroy(): void {
        this.allSubscriptions.forEach((subscription) =>
            subscription.unsubscribe()
        );
    }

    changeTheOptionsInTheAutocompleteDependingOnWhatIsTyped(): void {
        this.filteredMuscles = merge(
            this.forceAutocompleteOptionsFiltration,
            this.musclesControl.valueChanges
        ).pipe(
            startWith(''),
            map((valueTyped: string) => {
                valueTyped = valueTyped.toLowerCase();

                return this.notSelectedMuscles
                    .sort((muscle) => (muscle.startsWith(valueTyped) ? -1 : 1))
                    .filter((muscle) =>
                        muscle.toLowerCase().includes(valueTyped)
                    );
            })
        );
    }

    updateTheValueOfSelectedMuscleGroupsWhenTheControlToKeepUpdatedChanges(): void {
        this.allSubscriptions.push(
            merge(of([]), this.controlToKeepUpdated!.valueChanges).subscribe(
                (newControlValue) => {
                    if (Array.isArray(this.controlToKeepUpdated?.value)) {
                        this.selectedMuscles = this.controlToKeepUpdated!.value;
                        this.notSelectedMuscles = allMuscleGroups.filter(
                            (muscle) => !this.selectedMuscles.includes(muscle)
                        );
                    } else if (Array.isArray(newControlValue)) {
                        this.selectedMuscles = newControlValue;
                        this.notSelectedMuscles = allMuscleGroups.filter(
                            (muscle) => !this.selectedMuscles.includes(muscle)
                        );
                    }
                }
            )
        );
    }

    muscleRemoved(removedMuscle: string): void {
        const index = this.selectedMuscles.indexOf(removedMuscle);

        this.selectedMuscles.splice(index, 1);
        this.notSelectedMuscles.push(removedMuscle);
        this.controlToKeepUpdated?.setValue(this.selectedMuscles);
        this.forceAutocompleteOptionsFiltration.next('');
        this.controlToKeepUpdated?.setValue(this.selectedMuscles);
    }

    muscleSelected(event: MatAutocompleteSelectedEvent): void {
        const selectedMuscle = event.option.viewValue,
            index = this.notSelectedMuscles.indexOf(selectedMuscle);

        this.notSelectedMuscles.splice(index, 1);
        this.selectedMuscles.push(selectedMuscle);
        this.controlToKeepUpdated?.setValue(this.selectedMuscles);
        this.forceAutocompleteOptionsFiltration.next('');
        this.musclesInput.nativeElement.value = '';
    }
}
