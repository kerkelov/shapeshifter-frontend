import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Exercise } from '../../../main/exercises/models/exercise.model';
import { ExercisesSearch } from '../../models/exercises-search.model';

@Component({
    selector: 'app-search-exercises',
    templateUrl: './search-exercises.component.html',
    styleUrls: ['./search-exercises.component.scss']
})
export class SearchExercisesComponent {
    @Input()
    allowOnlyOneSelection: boolean = false;
    @Input()
    selectButtonLabel: string | undefined;
    @Output()
    done = new EventEmitter<Exercise[]>();
    currentSearch: ExercisesSearch;

    updateCurrentSearch(newExercises: ExercisesSearch): void {
        this.currentSearch = newExercises;
    }

    selectionDone(selection: Exercise[]): void {
        this.done.emit(selection);
    }
}
