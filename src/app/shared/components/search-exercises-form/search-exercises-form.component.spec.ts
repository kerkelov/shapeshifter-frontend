import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchExercisesFormComponent } from './search-exercises-form.component';

describe('SearchExercisesFormComponent', () => {
  let component: SearchExercisesFormComponent;
  let fixture: ComponentFixture<SearchExercisesFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchExercisesFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchExercisesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
