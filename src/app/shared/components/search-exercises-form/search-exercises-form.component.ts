import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormControl,
    FormGroup
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs';
import { IsNotEmptyArrayValidator } from '../../../core/utils/validation/is-not-empty-array.validator';
import { Exercise } from '../../../main/exercises/models/exercise.model';
import { ExercisesSearch } from '../../models/exercises-search.model';
import { SearchExercisesQuery } from '../../models/search-exercises-query.model';
import { ExercisesService } from '../../services/exercises.service';
@Component({
    selector: 'app-search-exercises-form',
    templateUrl: './search-exercises-form.component.html',
    styleUrls: ['./search-exercises-form.component.scss']
})
export class SearchExercisesFormComponent implements OnInit {
    searchExercisesForm: FormGroup;
    flagOptions: string[] = ['or', 'and'];
    @Output() newExercisesSearched = new EventEmitter<ExercisesSearch>();

    constructor(
        private formBuilder: FormBuilder,
        private snackBar: MatSnackBar,
        private exercisesService: ExercisesService
    ) {}

    get flagControl(): AbstractControl | null {
        return this.searchExercisesForm.get('flag');
    }

    get primaryMuscleGroupsControl(): AbstractControl | null {
        return this.searchExercisesForm.get('primaryMuscleGroups');
    }

    get secondaryMuscleGroupsControl(): AbstractControl | null {
        return this.searchExercisesForm.get('secondaryMuscleGroups');
    }

    ngOnInit(): void {
        this.setupTheSearchExercisesForm();
    }

    setupTheSearchExercisesForm(): void {
        this.searchExercisesForm = this.formBuilder.group({
            primaryMuscleGroups: new FormControl('', [
                IsNotEmptyArrayValidator()
            ]),
            secondaryMuscleGroups: new FormControl('', [
                IsNotEmptyArrayValidator()
            ]),
            flag: new FormControl('or')
        });
    }

    secondaryMusclesButtonClicked(): void {
        this.search({
            secondaryMuscleGroups: this.secondaryMuscleGroupsControl?.value,
            flag: this.flagControl?.value
        });
    }

    primaryMusclesButtonClicked(): void {
        this.search({
            primaryMuscleGroups: this.primaryMuscleGroupsControl?.value
        });
    }

    search(query: SearchExercisesQuery): void {
        const observer = {
            next: (exercises: Exercise[]) => {
                if (!exercises.length) {
                    this.snackBar.open('No such exercises yet', 'Close', {
                        panelClass: 'round-white-background'
                    });
                    return;
                }

                this.newExercisesSearched.emit({
                    results: exercises,
                    searchedQuery: query
                });
            },
            error: (httpError: HttpErrorResponse) => {
                this.snackBar.open(httpError.error.message, 'Close', {
                    panelClass: 'round-white-background'
                });
            }
        };

        this.exercisesService.getAll(query).pipe(take(1)).subscribe(observer);
    }
}
