import { SelectionModel } from '@angular/cdk/collections';
import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChange,
    SimpleChanges
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSelectionListChange } from '@angular/material/list';
import { map, Observable, startWith } from 'rxjs';
import { Exercise } from '../../../main/exercises/models/exercise.model';
import { ExercisesSearch } from '../../models/exercises-search.model';

@Component({
    selector: 'app-select-exercises',
    templateUrl: './select-exercises.component.html',
    styleUrls: ['./select-exercises.component.scss']
})
export class ListExercisesComponent implements OnInit, OnChanges {
    @Input()
    currentSearch: ExercisesSearch;
    @Input()
    allowOnlyOneSelection: boolean;
    @Input()
    selectButtonLabel: string | undefined;
    @Output()
    selectionDone = new EventEmitter<Exercise[]>();
    selection: SelectionModel<Exercise> = new SelectionModel(true);
    filterByNameControl: FormControl = new FormControl();
    exercisesAvailableForSelection: Observable<Exercise[]>;

    ngOnInit(): void {
        this.selectButtonLabel = this.selectButtonLabel ?? 'Select';
        this.updateTheExercisesAvailableForSelectionBasedOnWhatIsTyped('');
    }

    ngOnChanges(changes: SimpleChanges): void {
        const currentSearchChange: SimpleChange = changes['currentSearch'];

        if (!currentSearchChange.firstChange) {
            this.updateTheExercisesAvailableForSelectionBasedOnWhatIsTyped(
                this.filterByNameControl.value ?? ''
            );
        }
    }

    updateTheExercisesAvailableForSelectionBasedOnWhatIsTyped(
        startValue: string
    ): void {
        this.exercisesAvailableForSelection =
            this.filterByNameControl.valueChanges.pipe(
                startWith(startValue),
                map((valueTyped: string) => {
                    valueTyped = valueTyped.toLowerCase();

                    return this.currentSearch.results
                        .filter((exercise) =>
                            exercise.name.toLowerCase().includes(valueTyped)
                        )
                        .sort((exercise) =>
                            exercise.name.toLowerCase().startsWith(valueTyped)
                                ? -1
                                : 1
                        );
                })
            );
    }

    onSelectionChange(selectionListChangedEvent: MatSelectionListChange): void {
        selectionListChangedEvent.options.forEach((option) => {
            if (this.allowOnlyOneSelection && this.selection.isEmpty()) {
                this.selection.select(option.value);
                this.emitCurrentlySelectedExercises();
            } else if (option.selected) {
                this.selection.select(option.value);
            } else {
                this.selection.deselect(option.value);
            }
        });
    }

    onSelectAll(): void {
        this.selection.select(...this.currentSearch.results);
    }

    onClearAll(): void {
        this.selection.clear();
    }

    emitCurrentlySelectedExercises(): void {
        this.selectionDone.emit(this.selection.selected);
    }
}
