import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExercisesForSelectionComponent } from './select-exercises.component';

describe('SelectExercisesComponent', () => {
    let component: ListExercisesForSelectionComponent;
    let fixture: ComponentFixture<ListExercisesForSelectionComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ListExercisesForSelectionComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ListExercisesForSelectionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
