import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Output,
    ViewChild
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatButton } from '@angular/material/button';
import { year1950 } from '../../../core/constants/dates';
import { formatDate } from '../../../core/utils/format-date';
import { DateRange } from '../../models/date-range.model';

@Component({
    selector: 'app-date-range-picker',
    templateUrl: './date-range-picker.component.html',
    styleUrls: ['./date-range-picker.component.scss']
})
export class DateRangePickerComponent implements AfterViewInit {
    @Output()
    rangePicked = new EventEmitter<DateRange>();
    fromDateControl: FormControl = new FormControl(null, [Validators.required]);
    toDateControl: FormControl = new FormControl(null, [Validators.required]);

    @ViewChild('periodButtons') periodButtons: ElementRef;
    @ViewChild('months3') defaultButtonSelection: MatButton;
    private readonly defaultTimePeriodInMonths: number = 3;

    ngAfterViewInit(): void {
        this.makeActive(this.defaultButtonSelection);
        this.selectLastMonths(this.defaultTimePeriodInMonths);
    }

    selectLastMonths(monthsCount?: number): void {
        const today: Date = new Date(formatDate(new Date()));
        const startDate: Date = monthsCount
            ? new Date(
                  today.getFullYear(),
                  today.getMonth() - monthsCount,
                  today.getDay()
              )
            : year1950;

        this.rangePicked.emit({ startDate, endDate: today });
    }

    makeActive(buttonClicked: MatButton): void {
        const allPeriodButtons: Element[] = [
            ...this.periodButtons.nativeElement.children
        ];

        allPeriodButtons.forEach((button: Element) =>
            button.classList.remove('active')
        );

        buttonClicked._elementRef.nativeElement.classList.add('active');
    }

    selectClicked(): void {
        let startDate: Date = this.fromDateControl.value,
            endDate: Date = this.toDateControl.value;

        if (startDate > endDate) {
            const temp = startDate;
            startDate = endDate;
            endDate = temp;
        }

        this.rangePicked.emit({ startDate, endDate });
    }
}
