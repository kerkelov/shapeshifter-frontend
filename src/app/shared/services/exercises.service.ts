import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Exercise } from '../../main/exercises/models/exercise.model';
import { SearchExercisesQuery } from '../models/search-exercises-query.model';

@Injectable({
    providedIn: 'root'
})
export class ExercisesService {
    exercisesBackendUrl: string = `${environment.backendUrl}/exercises/`;

    constructor(private httpClient: HttpClient) {}

    getAll({
        primaryMuscleGroups,
        secondaryMuscleGroups,
        flag
    }: SearchExercisesQuery): Observable<Exercise[]> {
        const query: SearchExercisesQuery = {};

        if (primaryMuscleGroups) {
            query.primaryMuscleGroups = primaryMuscleGroups;
        } else if (secondaryMuscleGroups && flag) {
            query.secondaryMuscleGroups = secondaryMuscleGroups;
            query.flag = flag;
        }

        return this.httpClient.get<Exercise[]>(this.exercisesBackendUrl, {
            params: query
        });
    }
}
