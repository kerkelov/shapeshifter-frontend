import { AfterViewInit, Directive, ElementRef } from '@angular/core';

@Directive({
    selector: '[focus]'
})
export class FocusDirective implements AfterViewInit {
    constructor(private elementRef: ElementRef) {}

    ngAfterViewInit(): void {
        setTimeout(() => this.elementRef.nativeElement.focus(), 0);
    }
}
