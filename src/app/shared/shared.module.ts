import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatRadioModule } from '@angular/material/radio';
import { RouterModule } from '@angular/router';
import { DateRangePickerComponent } from './components/date-range-picker/date-range-picker.component';
import { ErrorMessageComponent } from './components/error-message/error-message.component';
import { SearchExercisesFormComponent } from './components/search-exercises-form/search-exercises-form.component';
import { SearchExercisesComponent } from './components/search-exercises/search-exercises.component';
import { ListExercisesComponent } from './components/select-exercises/select-exercises.component';
import { SelectMuscleGroupsComponent } from './components/select-muscle-groups/select-muscle-groups.component';
import { UploadWidgetComponent } from './components/upload-widget/upload-widget.component';
import { FocusDirective } from './directives/focus.directive';
import { AccessDeniedPageComponent } from './pages/access-denied-page/access-denied-page.component';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';

@NgModule({
    declarations: [
        ErrorMessageComponent,
        UploadWidgetComponent,
        NotFoundPageComponent,
        AccessDeniedPageComponent,
        SelectMuscleGroupsComponent,
        SearchExercisesFormComponent,
        SearchExercisesComponent,
        ListExercisesComponent,
        FocusDirective,
        DateRangePickerComponent
    ],
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        RouterModule,
        ReactiveFormsModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatRadioModule,
        MatListModule,
        MatCheckboxModule,
        MatDividerModule,
        ScrollingModule,
        MatDatepickerModule
    ],
    exports: [
        ErrorMessageComponent,
        UploadWidgetComponent,
        SelectMuscleGroupsComponent,
        SearchExercisesComponent,
        DateRangePickerComponent
    ]
})
export class SharedModule {}
