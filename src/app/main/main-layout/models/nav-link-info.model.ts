export type NavLinkInfo = {
    routeLink: string;
    icon: string;
    label: string;
    isAdminOnly?: boolean;
    isLogoutButton?: boolean;
};
