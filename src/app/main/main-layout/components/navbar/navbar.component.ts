import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../../core/services/auth.service';
import { CurrentUserService } from '../../../../core/services/current-user.service';
import { NavLinkInfo } from '../../models/nav-link-info.model';
import { navData } from './nav-data';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
    expanded: boolean = false;
    navData: NavLinkInfo[] = navData;

    constructor(
        private authService: AuthService,
        public currentUserService: CurrentUserService,
        private router: Router
    ) {}

    expand(): void {
        this.expanded = true;
    }

    collapse(): void {
        this.expanded = false;
    }

    logout(): void {
        this.authService.logout();
        this.currentUserService.clearCurrentUser();
        this.router.navigate(['public']);
    }
}
