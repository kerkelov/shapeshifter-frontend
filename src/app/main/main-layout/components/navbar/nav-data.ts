export const navData = [
    {
        routeLink: '/main/profile',
        icon: 'fa fa-id-card',
        label: 'Profile'
    },
    {
        routeLink: '/main/measurement-events',
        icon: 'fa fa-weight-scale',
        label: 'Measurements'
    },
    {
        routeLink: '/main/measurement-events/progress',
        icon: 'fa-solid fa-chart-line',
        label: 'Measurements progress'
    },

    //If your name is Nikol, the link below is commented only for the demo because it's currently an empty route

    //TODO: uncomment this
    // {
    //     routeLink: '/main/workouts/templates',
    //     icon: 'fa-solid fa-file-lines',
    //     label: 'Workout templates'
    // },
    {
        routeLink: '/main/workouts',
        icon: 'fa-solid fa-heart-pulse',
        label: 'Workouts'
    },
    {
        routeLink: '/main/workouts/progress',
        icon: 'fa-solid fa-square-poll-vertical',
        label: 'Exercises progress'
    },
    {
        routeLink: '/main/exercises',
        icon: 'fa-solid fa-dumbbell',
        label: 'Exercises',
        isAdminOnly: true
    },
    {
        routeLink: '/main/messages',
        icon: 'fa-solid fa-comments',
        label: 'Messages',
        isAdminOnly: true
    },
    {
        routeLink: '',
        icon: 'fa-solid fa-arrow-left',
        label: 'Logout',
        isLogoutButton: true
    }
];
