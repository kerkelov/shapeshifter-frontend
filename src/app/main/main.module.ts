import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { MainRoutingModule } from './main-routing.module';

@NgModule({
    declarations: [],
    imports: [CommonModule, SharedModule, MainRoutingModule]
})
export class MainModule {}
