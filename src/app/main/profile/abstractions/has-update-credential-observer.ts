import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ResponseUser } from '../../../core/models/response-user.model';
import { CurrentUserService } from '../../../core/services/current-user.service';

export abstract class HasUpdateCredentialObserver {
    constructor(
        private snackBar: MatSnackBar,
        protected currentUserService: CurrentUserService
    ) {}

    updateCredentialObserver = {
        next: (updatedUser: ResponseUser) => {
            this.currentUserService.setCurrentUser(updatedUser);
            this.snackBar.open('Your change was successfull!', 'Close', {
                panelClass: 'round-white-background'
            });
        },
        error: (httpError: HttpErrorResponse) => {
            this.snackBar.open(httpError.error.message, 'Close', {
                panelClass: 'round-white-background'
            });
        }
    };
}
