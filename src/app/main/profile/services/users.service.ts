import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { RegisterUser } from '../../../core/models/register-user.model';
import { ResponseUser } from '../../../core/models/response-user.model';

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    usersBackendUrl = `${environment.backendUrl}/users/`;

    constructor(private httpClient: HttpClient) {}

    getById(id: string): Observable<ResponseUser> {
        return this.httpClient.get<ResponseUser>(this.usersBackendUrl + id);
    }

    update(
        id: string,
        partsToUpdate: Partial<RegisterUser>,
        password: string | null
    ): Observable<ResponseUser> {
        return this.httpClient.patch<ResponseUser>(this.usersBackendUrl + id, {
            partsToUpdate,
            password
        });
    }

    delete(id: string, password: string): Observable<unknown> {
        return this.httpClient.delete<unknown>(this.usersBackendUrl + id, {
            body: { password }
        });
    }
}
