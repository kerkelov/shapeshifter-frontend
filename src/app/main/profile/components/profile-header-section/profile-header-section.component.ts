import { Component, OnDestroy, OnInit } from '@angular/core';
import { filter, Subscription } from 'rxjs';
import { CurrentUserService } from '../../../../core/services/current-user.service';

@Component({
    selector: 'app-profile-header-section',
    templateUrl: './profile-header-section.component.html',
    styleUrls: ['./profile-header-section.component.scss']
})
export class ProfileHeaderSectionComponent implements OnInit, OnDestroy {
    username: string;
    email: string;
    profilePictureUrl: string;
    allSubscriptions: Subscription[] = [];

    constructor(private currentUserService: CurrentUserService) {}

    ngOnInit(): void {
        this.allSubscriptions.push(
            this.currentUserService.currentUser
                .pipe(filter((currentUser) => currentUser !== null))
                .subscribe((currentUser) => {
                    this.username = currentUser!.username;
                    this.email = currentUser!.email;
                    this.profilePictureUrl = currentUser!.profilePictureUrl;
                })
        );
    }

    ngOnDestroy(): void {
        this.allSubscriptions.forEach((subscriptin) =>
            subscriptin.unsubscribe()
        );
    }
}
