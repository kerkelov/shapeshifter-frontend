import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs';
import { ResponseUser } from '../../../../core/models/response-user.model';
import { CurrentUserService } from '../../../../core/services/current-user.service';
import { HasUpdateCredentialObserver } from '../../abstractions/has-update-credential-observer';
import { ChangeCredentialSubmitResult } from '../../models/change-credential-submit-result.model';
import { Credential } from '../../models/credential.model';
import { UsersService } from '../../services/users.service';

@Component({
    selector: 'app-change-email',
    templateUrl: './change-email.component.html',
    styleUrls: ['./change-email.component.scss']
})
export class ChangeEmailComponent extends HasUpdateCredentialObserver {
    title: string = 'Change your email';
    credential: Credential = {
        name: 'email',
        validators: [Validators.required, Validators.email]
    };

    constructor(
        private usersService: UsersService,
        currentUserService: CurrentUserService,
        snackBar: MatSnackBar
    ) {
        super(snackBar, currentUserService);
    }

    submit(formData: ChangeCredentialSubmitResult): void {
        const currentUser: ResponseUser | null =
            this.currentUserService.getCurrentUser();

        this.usersService
            .update(
                currentUser?._id ?? '',
                { email: formData.credential },
                formData.currentPassword
            )
            .pipe(take(1))
            .subscribe(this.updateCredentialObserver);
    }
}
