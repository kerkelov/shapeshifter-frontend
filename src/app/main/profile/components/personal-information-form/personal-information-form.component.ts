import { HttpErrorResponse } from '@angular/common/http';
import {
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription, take } from 'rxjs';
import { ResponseUser } from '../../../../core/models/response-user.model';
import { CurrentUserService } from '../../../../core/services/current-user.service';
import { UsersService } from '../../services/users.service';

@Component({
    selector: 'app-personal-information-form',
    templateUrl: './personal-information-form.component.html',
    styleUrls: ['./personal-information-form.component.scss']
})
export class PersonalInformationFormComponent implements OnInit, OnDestroy {
    @Input() canEdit: boolean;
    @Output() setCanEditToFalse = new EventEmitter();
    userTemplate: ResponseUser | null;
    personalInformationForm: FormGroup;
    genders: string[] = ['male', 'female'];
    allSubscriptions: Subscription[] = [];
    updateObserver = {
        next: (updatedUser: ResponseUser) => {
            this.userTemplate = updatedUser;
            this.currentUserService.setCurrentUser(updatedUser);
        },
        error: (httpError: HttpErrorResponse) =>
            this.snackBar.open(httpError.error.message, 'Close', {
                panelClass: 'round-white-background'
            })
    };

    constructor(
        private formBuilder: FormBuilder,
        private snackBar: MatSnackBar,
        private usersService: UsersService,
        private currentUserService: CurrentUserService
    ) {}

    get usernameControl(): AbstractControl | null {
        return this.personalInformationForm.get('username');
    }

    get heightControl(): AbstractControl | null {
        return this.personalInformationForm.get('height');
    }

    ngOnInit(): void {
        this.userTemplate = this.currentUserService.getCurrentUser();
        this.setupThePersonalInformationForm();
    }

    ngOnDestroy(): void {
        this.allSubscriptions.forEach((subscription) =>
            subscription.unsubscribe()
        );
    }

    saveClicked(): void {
        this.usersService
            .update(
                this.currentUserService.getCurrentUser()!._id,
                this.personalInformationForm.value,
                null
            )
            .pipe(take(1))
            .subscribe(this.updateObserver);

        this.setCanEditToFalse.emit();
    }

    cancel(): void {
        this.setupThePersonalInformationForm();
        this.setCanEditToFalse.emit();
    }

    setupThePersonalInformationForm(): void {
        this.personalInformationForm = this.formBuilder.group({
            username: new FormControl(this.userTemplate?.username, [
                Validators.required,
                Validators.minLength(2),
                Validators.maxLength(25)
            ]),
            gender: new FormControl(this.userTemplate?.gender, [
                Validators.required
            ]),
            height: new FormControl(this.userTemplate?.height, [
                Validators.required,
                Validators.min(100),
                Validators.max(230)
            ]),
            dateOfBirth: new FormControl(this.userTemplate?.dateOfBirth, [
                Validators.required
            ])
        });
    }

    changeProfilePicture(newProfilePictureUrl: string): void {
        this.usersService
            .update(
                this.currentUserService.getCurrentUser()!._id,
                { profilePictureUrl: newProfilePictureUrl },
                null
            )
            .pipe(take(1))
            .subscribe(this.updateObserver);
    }
}
