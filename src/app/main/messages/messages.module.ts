import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MainLayoutModule } from '../main-layout/main-layout.module';
import { MessagesRoutingModule } from './messages-routing.module';
import { MessagesPageComponent } from './pages/messages-page/messages-page.component';

@NgModule({
    declarations: [MessagesPageComponent],
    imports: [
        CommonModule,
        MessagesRoutingModule,
        MainLayoutModule,
        MatSnackBarModule,
        MatButtonModule
    ]
})
export class MessagesModule {}
