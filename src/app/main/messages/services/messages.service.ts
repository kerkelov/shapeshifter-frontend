import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Message } from '../../../shared/models/message.model';

@Injectable({
    providedIn: 'root'
})
export class MessagesService {
    messagesBackendUrl: string = `${environment.backendUrl}/messages-to-admins/`;

    constructor(private httpClient: HttpClient) {}

    getAll(): Observable<Message[]> {
        return this.httpClient.get<Message[]>(this.messagesBackendUrl);
    }

    remove(id: string): Observable<unknown> {
        return this.httpClient.delete<unknown>(this.messagesBackendUrl + id);
    }
}
