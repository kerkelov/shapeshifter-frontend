import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs';
import { Message } from '../../../../shared/models/message.model';
import { MessagesService } from '../../services/messages.service';

@Component({
    selector: 'app-messages-page',
    templateUrl: './messages-page.component.html',
    styleUrls: ['./messages-page.component.scss']
})
export class MessagesPageComponent implements OnInit {
    messagesMap: Map<string, Message> = new Map();

    constructor(
        private messagesService: MessagesService,
        private snackBar: MatSnackBar
    ) {}

    get allMessages(): Message[] {
        return [...this.messagesMap.values()];
    }

    ngOnInit(): void {
        this.fetchTheMessages();
    }

    fetchTheMessages(): void {
        this.messagesService
            .getAll()
            .pipe(take(1))
            .subscribe((allMessagesResponse) => {
                allMessagesResponse.forEach((message) => {
                    if (!this.messagesMap.has(message._id)) {
                        this.messagesMap.set(message._id, message);
                    }
                });
            });
    }

    done(id: string): void {
        this.messagesService
            .remove(id)
            .pipe(take(1))
            .subscribe({
                next: () => this.messagesMap.delete(id),
                error: () =>
                    this.snackBar.open(
                        'Error happened, deletion failed',
                        'Close',
                        { panelClass: 'round-white-background' }
                    )
            });
    }
}
