import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsAdminGuard } from '../core/guards/is-admin.guard';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full'
    },
    {
        path: 'profile',
        loadChildren: () =>
            import('./profile/profile.module').then((m) => m.ProfileModule)
    },
    {
        path: 'measurement-events',
        loadChildren: () =>
            import('./measurement-events/measurement-events.module').then(
                (m) => m.MeasurementEventsModule
            )
    },
    {
        path: 'workouts',
        loadChildren: () =>
            import('./workouts/workouts.module').then((m) => m.WorkoutsModule)
    },
    {
        path: 'exercises',
        loadChildren: () =>
            import('./exercises/exercises.module').then(
                (m) => m.ExercisesModule
            ),
        canActivate: [IsAdminGuard],
        canActivateChild: [IsAdminGuard]
    },
    {
        path: 'messages',
        loadChildren: () =>
            import('./messages/messages.module').then((m) => m.MessagesModule),
        canActivate: [IsAdminGuard],
        canActivateChild: [IsAdminGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MainRoutingModule {}
