import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {
    MatDialogModule,
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../shared/shared.module';
import { MainLayoutModule } from '../main-layout/main-layout.module';
import { SearchExercisesDialogComponent } from './components/search-exercises-dialog/search-exercises-dialog.component';
import { SearchWorkoutComponent } from './components/search-workout/search-workout.component';
import { SearchWorkoutsComponent } from './components/search-workouts/search-workouts.component';
import { StartWorkoutComponent } from './components/start-workout/start-workout.component';
import { WorkoutCardComponent } from './components/workout-card/workout-card.component';
import { WorkoutsCalendarHeaderComponent } from './components/workouts-calendar-header/workouts-calendar-header.component';
import { WorkoutsForTheDayDialogComponent } from './components/workouts-for-the-day-dialog/workouts-for-the-day-dialog.component';
import { WorkoutsFormComponent } from './components/workouts-form/workouts-form.component';
import { ProgressPageComponent } from './pages/progress-page/progress-page.component';
import { TemplatesPageComponent } from './pages/templates-page/templates-page.component';
import { WorkoutsPageComponent } from './pages/workouts-page/workouts-page.component';
import { WorkoutsRoutingModule } from './workouts-routing.module';

@NgModule({
    providers: [
        { provide: MAT_DIALOG_DATA, useValue: null },
        { provide: MatDialogRef, useValue: null }
    ],
    declarations: [
        TemplatesPageComponent,
        ProgressPageComponent,
        WorkoutsPageComponent,
        WorkoutsFormComponent,
        SearchWorkoutsComponent,
        WorkoutsCalendarHeaderComponent,
        StartWorkoutComponent,
        SearchExercisesDialogComponent,
        WorkoutsForTheDayDialogComponent,
        WorkoutCardComponent,
        SearchWorkoutComponent
    ],
    imports: [
        CommonModule,
        WorkoutsRoutingModule,
        MatDividerModule,
        MatExpansionModule,
        MatCardModule,
        MainLayoutModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatTabsModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatNativeDateModule,
        NgxChartsModule,
        MatListModule,
        MatDividerModule,
        SharedModule
    ]
})
export class WorkoutsModule {}
