import { Component, EventEmitter, OnDestroy, Output } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Exercise } from '../../../exercises/models/exercise.model';
import { ExerciseExecution } from '../../models/exercise-execution.model';
import { Workout } from '../../models/workout.model';
import { SearchExercisesDialogComponent } from '../search-exercises-dialog/search-exercises-dialog.component';

@Component({
    selector: 'app-start-workout',
    templateUrl: './start-workout.component.html',
    styleUrls: ['./start-workout.component.scss']
})
export class StartWorkoutComponent implements OnDestroy {
    @Output()
    workoutWithNoSetsCreated = new EventEmitter<Workout>();
    allSubscriptions: Subscription[] = [];

    constructor(private dialog: MatDialog) {}

    ngOnDestroy(): void {
        this.allSubscriptions.forEach((subscription) =>
            subscription.unsubscribe()
        );
    }

    openSearchExercisesDialog(): void {
        const dialogRef = this.dialog.open(SearchExercisesDialogComponent, {
            autoFocus: false,
            width: '70vw',
            panelClass: 'round-without-padding'
        });

        this.handleDialogClosing(dialogRef);
    }

    handleDialogClosing(
        dialog: MatDialogRef<SearchExercisesDialogComponent>
    ): void {
        this.allSubscriptions.push(
            dialog.afterClosed().subscribe((exercisesSelected: Exercise[]) => {
                const workoutWithNoSets: Workout | null =
                    this.turnIntoWorkoutWithNoSets(exercisesSelected);

                if (workoutWithNoSets) {
                    this.workoutWithNoSetsCreated.emit(workoutWithNoSets);
                }
            })
        );
    }

    turnIntoWorkoutWithNoSets(
        exercises: Exercise[] | undefined
    ): Workout | null {
        if (!exercises) return null;
        const result: Workout = {} as Workout;

        result.exerciseExecutions = exercises.map(
            (exercise: Exercise): ExerciseExecution => {
                return { exercise, sets: [], _id: '' };
            }
        );

        //TODO: maybe add more stuff to the result here
        return result as Workout;
    }
}
