import {
    Component,
    EventEmitter,
    Inject,
    Input,
    OnInit,
    Output
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Workout } from '../../models/workout.model';

@Component({
    selector: 'app-workout-card',
    templateUrl: './workout-card.component.html',
    styleUrls: ['./workout-card.component.scss']
})
export class WorkoutCardComponent implements OnInit {
    @Input()
    formWorkoutTemplate: Workout | null;
    @Output() closed = new EventEmitter<string>();

    constructor(
        @Inject(MAT_DIALOG_DATA)
        private workoutTemplate: Workout | null,
        private dialogRef: MatDialogRef<WorkoutCardComponent> | null
    ) {}

    ngOnInit(): void {
        this.formWorkoutTemplate =
            this.formWorkoutTemplate ?? this.workoutTemplate;
    }

    currentWorkoutClosed(): void {
        this.dialogRef?.close();
        this.closed.emit(this.formWorkoutTemplate?._id);
    }
}
