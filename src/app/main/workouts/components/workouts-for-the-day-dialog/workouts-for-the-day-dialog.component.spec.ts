import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutsForTheDayDialogComponent } from './workouts-for-the-day-dialog.component';

describe('WorkoutsForTheDayDialogComponent', () => {
  let component: WorkoutsForTheDayDialogComponent;
  let fixture: ComponentFixture<WorkoutsForTheDayDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkoutsForTheDayDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkoutsForTheDayDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
