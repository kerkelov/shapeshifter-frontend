import { Component, Inject } from '@angular/core';
import {
    MatDialog,
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { Workout } from '../../models/workout.model';
import { WorkoutCardComponent } from '../workout-card/workout-card.component';

@Component({
    selector: 'app-workouts-for-the-day-dialog',
    templateUrl: './workouts-for-the-day-dialog.component.html',
    styleUrls: ['./workouts-for-the-day-dialog.component.scss']
})
export class WorkoutsForTheDayDialogComponent {
    constructor(
        @Inject(MAT_DIALOG_DATA)
        public workouts: Workout[],
        private dialogRef: MatDialogRef<WorkoutsForTheDayDialogComponent>,
        private dialog: MatDialog
    ) {}

    workoutClicked(index: number): void {
        this.dialogRef.close();

        this.dialog.open(WorkoutCardComponent, {
            data: this.workouts[index],
            autoFocus: false,
            panelClass: 'round-without-padding'
        });
    }
}
