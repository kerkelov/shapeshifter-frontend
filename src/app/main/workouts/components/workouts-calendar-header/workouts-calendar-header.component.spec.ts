import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutsCalendarHeaderComponent } from './workouts-calendar-header.component';

describe('WorkoutsCalendarHeaderComponent', () => {
    let component: WorkoutsCalendarHeaderComponent;
    let fixture: ComponentFixture<WorkoutsCalendarHeaderComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [WorkoutsCalendarHeaderComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(WorkoutsCalendarHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
