import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { MatCalendar } from '@angular/material/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { delay, Subscription, take } from 'rxjs';
import { formatDate } from '../../../../core/utils/format-date';
import { WorkoutUpdateInfo } from '../../models/workout-update-info.model';
import { Workout } from '../../models/workout.model';
import { WorkoutsService } from '../../services/workouts.service';
import { WorkoutCardComponent } from '../workout-card/workout-card.component';
import { WorkoutsForTheDayDialogComponent } from '../workouts-for-the-day-dialog/workouts-for-the-day-dialog.component';

@Component({
    selector: 'app-workouts-calendar-header',
    templateUrl: './workouts-calendar-header.component.html',
    styleUrls: ['./workouts-calendar-header.component.scss']
})
export class WorkoutsCalendarHeaderComponent implements OnInit {
    currentMonthData: Map<string, Workout[]> = new Map();
    isShowingSpinner: boolean;
    today: Date = new Date();
    allSubscriptions: Subscription[] = [];

    constructor(
        private calendar: MatCalendar<Date>,
        private dateAdapter: DateAdapter<Date>,
        private workoutsService: WorkoutsService,
        private snackBar: MatSnackBar,
        private dialog: MatDialog
    ) {}

    get periodLabel(): string {
        const dateString = this.calendar.activeDate.toDateString(),
            dateStringParts = dateString.split(' '),
            month = dateStringParts[1],
            year = dateStringParts[3];

        return `${month} ${year}`;
    }

    ngOnInit(): void {
        this.calendar.dateFilter = (date: Date) =>
            !!this.currentMonthData.get(formatDate(date));

        this.openDialogWhenDateIsSelected();
        this.getNotifiedWhenWorkoutIsUpdated();
        this.refreshWhatIsSelectable();
    }

    ngOnDestroy(): void {
        this.allSubscriptions.forEach(
            (subscription) => subscription.unsubscribe
        );
    }

    refreshWhatIsSelectable(): void {
        const previousActiveDate = this.calendar.activeDate;

        this.calendar._goToDateInView(
            this.dateAdapter.addCalendarYears(this.calendar.activeDate, -50),
            'month'
        );

        this.loadNewMonth(previousActiveDate);
    }

    headerButtonClicked(
        period: 'month' | 'year',
        action: 'next' | 'previous'
    ): void {
        let newActiveDate: Date;
        const amount = action === 'next' ? 1 : -1;

        if (period === 'year') {
            newActiveDate = this.dateAdapter.addCalendarYears(
                this.calendar.activeDate,
                amount
            );
        } else {
            newActiveDate = this.dateAdapter.addCalendarMonths(
                this.calendar.activeDate,
                amount
            );
        }

        this.loadNewMonth(newActiveDate);
    }

    openDialogWhenDateIsSelected(): void {
        this.allSubscriptions.push(
            this.calendar.selectedChange.subscribe((newDate: Date) => {
                const workoutsForThatDate: Workout[] =
                    this.currentMonthData.get(formatDate(newDate)) ?? [];

                if (workoutsForThatDate.length > 1) {
                    this.dialog.open(WorkoutsForTheDayDialogComponent, {
                        width: '60vw',
                        autoFocus: false,
                        panelClass: 'round-without-padding',
                        data: workoutsForThatDate
                    });
                } else {
                    this.dialog.open(WorkoutCardComponent, {
                        width: '60vw',
                        autoFocus: false,
                        panelClass: 'round-without-padding',
                        data: workoutsForThatDate[0]
                    });
                }
            })
        );
    }

    loadNewMonth(someDateOfThatMonth: Date): void {
        this.isShowingSpinner = true;

        const newMonthFetchedObserver = {
            next: (newMonthData: Map<string, Workout[]>) => {
                this.currentMonthData = newMonthData;
                this.calendar.activeDate = someDateOfThatMonth;
                this.isShowingSpinner = false;
            },
            error: (httpError: HttpErrorResponse) => {
                this.snackBar.open(httpError.error.message, 'Close', {
                    panelClass: 'round-white-background'
                });
            }
        };

        this.workoutsService
            .fetchAllWorkoutsForMonth(someDateOfThatMonth)
            .pipe(take(1))
            .pipe(delay(500))
            .subscribe(newMonthFetchedObserver);
    }

    removeWorkoutFromCurrentMonthData(workoutToRemove: Workout): void {
        const workoutDate: string = formatDate(new Date(workoutToRemove.date)),
            workoutArrayIndex = this.currentMonthData
                .get(workoutDate)
                ?.findIndex((workout) => workout._id === workoutToRemove._id);

        if (workoutArrayIndex !== undefined) {
            this.currentMonthData
                .get(workoutDate)
                ?.splice(workoutArrayIndex, 1);
        }
    }

    getNotifiedWhenWorkoutIsUpdated(): void {
        this.allSubscriptions.push(
            this.workoutsService.workoutUpdated.subscribe(
                (info: WorkoutUpdateInfo) => {
                    const workoutDate: string = formatDate(
                            new Date(info.workout.date)
                        ),
                        month: number = parseInt(workoutDate.split('-')[0]),
                        year: number = parseInt(workoutDate.split('-')[2]),
                        calendarMonth: number =
                            this.calendar.activeDate.getMonth() + 1,
                        calendarYear: number =
                            this.calendar.activeDate.getFullYear();

                    if (month !== calendarMonth || year !== calendarYear) {
                        return;
                    }

                    if (info.action === 'DELETE') {
                        this.removeWorkoutFromCurrentMonthData(info.workout);
                    } else {
                        this.removeWorkoutFromCurrentMonthData(info.workout);

                        if (!this.currentMonthData.has(workoutDate)) {
                            this.currentMonthData.set(workoutDate, []);
                        }

                        this.currentMonthData
                            .get(workoutDate)
                            ?.push(info.workout);
                    }

                    if (info.action === 'DELETE' || info.action === 'CREATE') {
                        this.refreshWhatIsSelectable();
                    }
                }
            )
        );
    }
}
