import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchWorkoutComponent } from './search-workout.component';

describe('SearchWorkoutComponent', () => {
  let component: SearchWorkoutComponent;
  let fixture: ComponentFixture<SearchWorkoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchWorkoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchWorkoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
