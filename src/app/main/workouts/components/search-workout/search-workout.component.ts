import { Component, OnDestroy, OnInit } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormControl,
    FormGroup
} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription, take } from 'rxjs';
import { IsNotEmptyArrayValidator } from '../../../../core/utils/validation/is-not-empty-array.validator';
import { DateRange } from '../../../../shared/models/date-range.model';
import { Exercise } from '../../../exercises/models/exercise.model';
import { SearchWorkoutQuery } from '../../models/search-workout-query.model';
import { Workout } from '../../models/workout.model';
import { WorkoutsService } from '../../services/workouts.service';
import { SearchExercisesDialogComponent } from '../search-exercises-dialog/search-exercises-dialog.component';
import { WorkoutsForTheDayDialogComponent } from '../workouts-for-the-day-dialog/workouts-for-the-day-dialog.component';

@Component({
    selector: 'app-search-workout',
    templateUrl: './search-workout.component.html',
    styleUrls: ['./search-workout.component.scss']
})
export class SearchWorkoutComponent implements OnInit, OnDestroy {
    searchWorkoutsForm: FormGroup;
    allSubscriptions: Subscription[] = [];

    constructor(
        private formBuilder: FormBuilder,
        private dialog: MatDialog,
        private workoutsService: WorkoutsService,
        private snackBar: MatSnackBar
    ) {}

    get musclesControl(): AbstractControl | null {
        return this.searchWorkoutsForm.get('muscles');
    }

    get startDateControl(): AbstractControl | null {
        return this.searchWorkoutsForm.get('startDate');
    }

    get endDateControl(): AbstractControl | null {
        return this.searchWorkoutsForm.get('endDate');
    }

    ngOnInit(): void {
        this.setupTheExercisesForm();
    }

    ngOnDestroy(): void {
        this.allSubscriptions.forEach((subscription) => {
            subscription.unsubscribe();
        });
    }

    setupTheExercisesForm(): void {
        this.searchWorkoutsForm = this.formBuilder.group({
            muscles: new FormControl([], [IsNotEmptyArrayValidator()]),
            startDate: new FormControl(null),
            endDate: new FormControl(null)
        });
    }

    saveTimeFrame(timeFrame: DateRange): void {
        this.startDateControl?.setValue(timeFrame.startDate);
        this.endDateControl?.setValue(timeFrame.endDate);
    }

    openSearchExercisesDialog(): void {
        const dialogRef = this.dialog.open(SearchExercisesDialogComponent, {
            data: { selectButtonLabel: 'Search' },
            autoFocus: false,
            width: '70vw',
            panelClass: 'round-without-padding'
        });

        this.handleSearchExercisesDialogClosing(dialogRef);
    }

    handleSearchExercisesDialogClosing(
        dialog: MatDialogRef<SearchExercisesDialogComponent>
    ): void {
        this.allSubscriptions.push(
            dialog.afterClosed().subscribe((exercisesSelected: Exercise[]) => {
                const query: SearchWorkoutQuery = {
                    startDate: this.startDateControl?.value,
                    endDate: this.endDateControl?.value,
                    exercisesPerformed: exercisesSelected
                };

                this.triggerSearch(query);
            })
        );
    }

    searchByMuscleGroupsClicked(): void {
        const query: SearchWorkoutQuery = {
            startDate: this.startDateControl?.value,
            endDate: this.endDateControl?.value,
            musclesEngaged: this.musclesControl?.value
        };

        this.triggerSearch(query);
    }

    triggerSearch(searchWorkoutsQuery: SearchWorkoutQuery): void {
        this.workoutsService
            .searchWorkouts(searchWorkoutsQuery)
            .pipe(take(1))
            .subscribe((workoutResults: Workout[]) => {
                if (!workoutResults.length) {
                    this.snackBar.open('No such workouts yet', 'Close', {
                        panelClass: 'round-white-background'
                    });

                    return;
                }

                this.openWorkoutsForTheDayDialog(workoutResults);
            });
    }

    openWorkoutsForTheDayDialog(workouts: Workout[]): void {
        const dialogRef = this.dialog.open(WorkoutsForTheDayDialogComponent, {
            data: workouts,
            autoFocus: false,
            width: '70vw',
            panelClass: 'round-without-padding'
        });
    }
}
