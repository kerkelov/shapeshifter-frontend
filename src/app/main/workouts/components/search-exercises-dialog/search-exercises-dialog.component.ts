import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Exercise } from '../../../exercises/models/exercise.model';

@Component({
    selector: 'app-search-exercises-dialog',
    templateUrl: './search-exercises-dialog.component.html',
    styleUrls: ['./search-exercises-dialog.component.scss']
})
export class SearchExercisesDialogComponent {
    @Output()
    done = new EventEmitter<Exercise[]>();

    constructor(
        private dialog: MatDialogRef<SearchExercisesDialogComponent>,
        @Inject(MAT_DIALOG_DATA)
        public data: {
            selectButtonLabel?: string;
            allowOnlyOneSelection?: boolean;
        } | null
    ) {}

    emit(exercises: Exercise[]): void {
        this.done.emit(exercises);
        this.dialog.close(exercises);
    }
}
