import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchExercisesDialogComponent } from './search-exercises-dialog.component';

describe('SearchExercisesDialogComponent', () => {
    let component: SearchExercisesDialogComponent;
    let fixture: ComponentFixture<SearchExercisesDialogComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [SearchExercisesDialogComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(SearchExercisesDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
