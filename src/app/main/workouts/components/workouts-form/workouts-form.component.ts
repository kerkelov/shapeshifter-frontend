import { HttpErrorResponse } from '@angular/common/http';
import {
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core';
import {
    AbstractControl,
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NavigationStart, Router } from '@angular/router';
import { debounceTime, filter, Subscription, take } from 'rxjs';
import { Exercise } from '../../../exercises/models/exercise.model';
import { ExerciseExecution } from '../../models/exercise-execution.model';
import { SetOfExercise } from '../../models/set-of-exercise.model';
import { Workout } from '../../models/workout.model';
import { TabRedirectionService } from '../../services/tab-redirection.service';
import { WorkoutInProgressService } from '../../services/workout-in-progress.service';
import { WorkoutsService } from '../../services/workouts.service';
import { SearchExercisesDialogComponent } from '../search-exercises-dialog/search-exercises-dialog.component';

@Component({
    selector: 'app-workouts-form',
    templateUrl: './workouts-form.component.html',
    styleUrls: ['./workouts-form.component.scss']
})
export class WorkoutsFormComponent implements OnInit, OnDestroy {
    @Input()
    workoutFormTemplate: Workout | null;
    strengthSetsPlaceholders: string[] = ['Kilograms', 'Reps'];
    exerciseExecutions: Map<Exercise, FormArray> = new Map();
    exercisesThatShouldBeExpanded: Set<Exercise> = new Set();
    allSubscriptions: Subscription[] = [];
    @Output() done = new EventEmitter();

    constructor(
        private formBuilder: FormBuilder,
        private snackBar: MatSnackBar,
        private workoutsService: WorkoutsService,
        private router: Router,
        private workoutInProgressService: WorkoutInProgressService,
        private dialog: MatDialog,
        private tabRedirectionService: TabRedirectionService
    ) {}

    get exercises(): Exercise[] {
        return [...this.exerciseExecutions.keys()];
    }

    ngOnInit(): void {
        this.addTheWorkoutFormTemplateDataToExerciseExecutions();
        this.saveTheCurrentWorkoutDataWhenRouteChanges();
    }

    ngOnDestroy(): void {
        this.allSubscriptions.forEach((subscription) =>
            subscription.unsubscribe()
        );
    }

    saveTheCurrentWorkoutDataWhenRouteChanges(): void {
        this.allSubscriptions.push(
            this.router.events
                .pipe(filter((event) => event instanceof NavigationStart))
                .subscribe(this.saveWorkoutData)
        );
    }

    saveWorkoutData(): void {
        if (this.workoutFormTemplate?._id) {
            return;
        }

        const theWorkoutDataUntilNow = this.turnExerciseExecutionsIntoWorkout();

        if (theWorkoutDataUntilNow.exerciseExecutions.length) {
            this.workoutInProgressService.setCurrentWorkout(
                theWorkoutDataUntilNow
            );
        }
    }

    removeExerciseExecution(exercise: Exercise): void {
        this.exerciseExecutions.delete(exercise);
    }

    addExerciseExecution(exercise: Exercise): void {
        if (!this.exerciseExecutions.has(exercise)) {
            this.exerciseExecutions.set(exercise, new FormArray([]));
        }
    }

    getFormControls(control: AbstractControl): FormControl[] {
        const result: FormControl[] = [],
            formGroup = control as FormGroup;

        for (const key in formGroup.controls) {
            result.push(formGroup.controls[key] as FormControl);
        }

        return result;
    }

    addTheWorkoutFormTemplateDataToExerciseExecutions(): void {
        this.workoutFormTemplate?.exerciseExecutions.forEach(
            (exerciseExecution) => {
                if (typeof exerciseExecution.exercise === 'string') {
                    return;
                }

                const formArray = new FormArray([]);

                if (exerciseExecution.exercise.isCardio) {
                    exerciseExecution.sets.forEach((set) =>
                        formArray.push(this.getNewCardioFormGroup(set))
                    );
                } else {
                    exerciseExecution.sets.forEach((set) =>
                        formArray.push(this.getNewStrengthFormGroup(set))
                    );
                }

                this.exerciseExecutions.set(
                    exerciseExecution.exercise,
                    formArray
                );
            }
        );
    }

    removeSet(exercise: Exercise, setIndex: number): void {
        this.exerciseExecutions.get(exercise)?.controls[0];
        this.exerciseExecutions.get(exercise)?.removeAt(setIndex);
        this.saveWorkoutData();
    }

    addSet(exercise: Exercise): void {
        if (exercise.isCardio) {
            this.exerciseExecutions
                .get(exercise)
                ?.push(this.getNewCardioFormGroup());
        } else {
            this.exerciseExecutions
                .get(exercise)
                ?.push(this.getNewStrengthFormGroup());
        }

        this.saveWorkoutData();
    }

    getNewCardioFormGroup(options?: {
        time?: number;
        distance?: number;
    }): FormGroup {
        const cardioFormGroup = this.formBuilder.group({
            time: new FormControl(options?.time, [
                Validators.required,
                Validators.max(180)
            ]),
            distance: new FormControl(options?.distance, [Validators.required])
        });

        this.getFormControls(cardioFormGroup).forEach((formControl) =>
            this.allSubscriptions.push(
                formControl.valueChanges
                    .pipe(debounceTime(2000))
                    .subscribe(() => this.saveWorkoutData())
            )
        );

        return cardioFormGroup;
    }

    getNewStrengthFormGroup(options?: {
        reps?: number;
        kilograms?: number;
    }): FormGroup {
        const strengthFormGroup = this.formBuilder.group({
            reps: new FormControl(options?.reps, [
                Validators.required,
                Validators.max(500)
            ]),
            kilograms: new FormControl(options?.kilograms, [
                Validators.required,
                Validators.max(1000)
            ])
        });

        this.getFormControls(strengthFormGroup).forEach((formControl) =>
            this.allSubscriptions.push(
                formControl.valueChanges
                    .pipe(debounceTime(2000))
                    .subscribe(() => this.saveWorkoutData)
            )
        );

        return strengthFormGroup;
    }

    openSearchExercisesDialog(): void {
        const dialogRef = this.dialog.open(SearchExercisesDialogComponent, {
            autoFocus: false,
            width: '70vw',
            panelClass: 'round-without-padding'
        });

        this.handleDialogClosing(dialogRef);
    }

    handleDialogClosing(
        dialog: MatDialogRef<SearchExercisesDialogComponent>
    ): void {
        this.allSubscriptions.push(
            dialog.afterClosed().subscribe((exercisesSelected: Exercise[]) => {
                const namesOfTheExercisesAlreadyPresent: Set<string> = new Set(
                    [...this.exerciseExecutions.keys()].map(
                        (exercise) => exercise.name
                    )
                );

                exercisesSelected.forEach((exercise) => {
                    if (!namesOfTheExercisesAlreadyPresent.has(exercise.name)) {
                        this.exerciseExecutions.set(
                            exercise,
                            new FormArray([])
                        );
                    }
                });
            })
        );
    }

    canTheFormBeSubmitted(): boolean {
        let hasErrorAnywhere = false;

        this.exerciseExecutions.forEach(
            (formArray: FormArray, key: Exercise) => {
                formArray.controls.forEach((control: AbstractControl) => {
                    const formGroup = control as FormGroup;

                    this.getFormControls(formGroup).forEach(
                        (formControl: FormControl) => {
                            if (!formControl.valid) {
                                hasErrorAnywhere = true;
                                formControl.markAsTouched();
                                this.exercisesThatShouldBeExpanded.add(key);
                            } else {
                                this.exercisesThatShouldBeExpanded.delete(key);
                            }
                        }
                    );
                });
            }
        );

        return !hasErrorAnywhere;
    }

    saveClicked(): void {
        if (!this.canTheFormBeSubmitted()) {
            this.snackBar.open('You have an empty set somewhere', 'Close', {
                panelClass: 'round-white-background'
            });

            return;
        }

        const observer = {
                next: (workoutResponse: Workout) => {
                    this.snackBar.open(
                        `Successfully ${
                            this.workoutFormTemplate?._id ? 'edited' : 'created'
                        } workout`,
                        'Close',
                        { panelClass: 'round-white-background' }
                    );

                    this.workoutsService.workoutUpdated.next({
                        workout: workoutResponse,
                        action: this.workoutFormTemplate?._id
                            ? 'UPDATE'
                            : 'CREATE'
                    });

                    this.done.emit();
                },
                error: (httpError: HttpErrorResponse) => {
                    this.snackBar.open(httpError.error.message, 'Close', {
                        panelClass: 'round-white-background'
                    });
                }
            },
            workout = this.turnExerciseExecutionsIntoWorkout();

        if (this.workoutFormTemplate?._id) {
            workout._id = this.workoutFormTemplate._id;
        }

        this.workoutsService.create(workout).pipe(take(1)).subscribe(observer);
    }

    turnExerciseExecutionsIntoWorkout(): Workout {
        const workoutResult: Workout = {} as Workout,
            exerciseExecutionsResult: ExerciseExecution[] = [],
            map = this.exerciseExecutions;

        for (const exercise of map.keys()) {
            const sets: FormArray | undefined = map.get(exercise),
                setsResult: SetOfExercise[] = [];

            sets?.controls.forEach((set: AbstractControl) => {
                const formGroup = set as FormGroup,
                    setResult: SetOfExercise = {},
                    [firstMetricControl, secondMetricControl] =
                        this.getFormControls(formGroup);

                if (exercise.isCardio) {
                    setResult.time = firstMetricControl.value;
                    setResult.distance = secondMetricControl.value;
                } else {
                    setResult.kilograms = firstMetricControl.value;
                    setResult.reps = secondMetricControl.value;
                }

                setsResult.push(setResult);
            });

            exerciseExecutionsResult.push({
                _id: '',
                exercise,
                sets: setsResult
            });
        }

        workoutResult.date = new Date();
        workoutResult.exerciseExecutions = exerciseExecutionsResult;

        return workoutResult;
    }

    deleteClicked(): void {
        if (!this.workoutFormTemplate) {
            return;
        }

        this.workoutsService
            .remove(this.workoutFormTemplate)
            .pipe(take(1))
            .subscribe(() => {
                this.snackBar.open('Successfully deleted a workout', 'Close', {
                    panelClass: 'round-white-background'
                });

                this.workoutsService.workoutUpdated.next({
                    workout: this.workoutFormTemplate ?? ({} as Workout),
                    action: 'DELETE'
                });

                this.done.emit();
            });
    }

    copyClicked(): void {
        const currentWorkoutInProgress: Workout | null =
            this.workoutInProgressService.getCurrentWorkout();

        if (currentWorkoutInProgress) {
            this.snackBar.open('Cannot copy, workout in progress', 'Close', {
                panelClass: 'round-white-background'
            });
            return;
        }

        this.workoutInProgressService.setCurrentWorkout(
            this.turnExerciseExecutionsIntoWorkout()
        );

        this.tabRedirectionService.requests.next('create-workout');
        this.done.emit();
    }
}
