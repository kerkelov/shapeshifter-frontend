export type WorkoutTemplate = {
    _id: string;
    name: string;
    exercises: string[];
};
