import { ExerciseExecution } from './exercise-execution.model';
import { WorkoutTemplate } from './workout-template.model';

export type Workout = {
    _id: string;
    workoutTemplate?: WorkoutTemplate;
    exerciseExecutions: ExerciseExecution[];
    date: Date;
    user: string;
};
