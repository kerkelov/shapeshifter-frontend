export type TabName =
    | 'create-workout'
    | 'workouts-calendar'
    | 'search-workouts';
