import { Workout } from './workout.model';

export type WorkoutUpdateInfo = {
    workout: Workout;
    action?: 'UPDATE' | 'DELETE' | 'CREATE';
};
