import { Exercise } from '../../exercises/models/exercise.model';

export type SearchWorkoutQuery = {
    startDate: Date;
    endDate: Date;
    musclesEngaged?: string[];
    exercisesPerformed?: Exercise[];
};
