import { Exercise } from '../../exercises/models/exercise.model';
import { SetOfExercise } from './set-of-exercise.model';

export type ExerciseExecution = {
    _id: string;
    exercise: Exercise | string;
    sets: SetOfExercise[];
};
