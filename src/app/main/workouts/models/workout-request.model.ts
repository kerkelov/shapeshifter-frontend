export type WorkoutRequest = {
    workoutTemplate?: string;
    exerciseExecutions: string[];
    date: Date;
    user: string;
};
