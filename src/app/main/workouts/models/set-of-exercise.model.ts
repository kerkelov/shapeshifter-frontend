export type SetOfExercise = {
    kilograms?: number;
    reps?: number;
    time?: number;
    distance?: number;
    note?: string;
};
