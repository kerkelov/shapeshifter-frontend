import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgressPageComponent } from './pages/progress-page/progress-page.component';
import { TemplatesPageComponent } from './pages/templates-page/templates-page.component';
import { WorkoutsPageComponent } from './pages/workouts-page/workouts-page.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: WorkoutsPageComponent
    },
    {
        path: 'templates',
        component: TemplatesPageComponent
    },
    {
        path: 'progress',
        component: ProgressPageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WorkoutsRoutingModule {}
