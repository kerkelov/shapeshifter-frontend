import { TestBed } from '@angular/core/testing';

import { ExerciseExecutionsService } from './exercise-executions.service';

describe('ExerciseExecutionsService', () => {
  let service: ExerciseExecutionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExerciseExecutionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
