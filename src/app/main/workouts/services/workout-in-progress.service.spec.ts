import { TestBed } from '@angular/core/testing';

import { WorkoutInProgressService } from './workout-in-progress.service';

describe('WorkoutInProgressService', () => {
  let service: WorkoutInProgressService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WorkoutInProgressService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
