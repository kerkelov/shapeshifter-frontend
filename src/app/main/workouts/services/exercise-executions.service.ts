import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ExerciseExecution } from '../models/exercise-execution.model';

@Injectable({
    providedIn: 'root'
})
export class ExerciseExecutionsService {
    exerciseExecutionsBackendUrl: string = `${environment.backendUrl}/exercise-executions/`;

    constructor(private httpClient: HttpClient) {}

    create(
        exerciseExecution: ExerciseExecution
    ): Observable<ExerciseExecution> {
        const exerciseExecutionRequest: ExerciseExecution = {
            ...exerciseExecution,
            exercise:
                typeof exerciseExecution.exercise === 'string'
                    ? exerciseExecution.exercise
                    : exerciseExecution.exercise._id
        };

        return this.httpClient.post<ExerciseExecution>(
            this.exerciseExecutionsBackendUrl,
            exerciseExecutionRequest
        );
    }

    createMany(
        exerciseExecutions: ExerciseExecution[]
    ): Observable<ExerciseExecution[]> {
        const allObservables: Observable<ExerciseExecution>[] = [];

        exerciseExecutions.forEach((exerciseExecution) => {
            allObservables.push(this.create(exerciseExecution));
        });

        return forkJoin(allObservables);
    }

    getById(id: string): Observable<ExerciseExecution> {
        return this.httpClient.get<ExerciseExecution>(
            this.exerciseExecutionsBackendUrl + id
        );
    }

    update(
        id: string,
        partsToUpdate: Partial<ExerciseExecution>
    ): Observable<ExerciseExecution> {
        return this.httpClient.patch<ExerciseExecution>(
            this.exerciseExecutionsBackendUrl + id,
            partsToUpdate
        );
    }

    remove(id: string): Observable<unknown> {
        return this.httpClient.delete<unknown>(
            this.exerciseExecutionsBackendUrl + id
        );
    }

    removeMany(exerciseExecutions: ExerciseExecution[]): Observable<unknown[]> {
        const allObservables: Observable<unknown>[] = [];
        exerciseExecutions.forEach((exerciseExecution) =>
            allObservables.push(this.remove(exerciseExecution._id ?? ''))
        );

        return forkJoin(allObservables);
    }
}
