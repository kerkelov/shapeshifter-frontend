import { TestBed } from '@angular/core/testing';

import { TabRedirectionService } from './tab-redirection.service';

describe('TabRedirectionService', () => {
    let service: TabRedirectionService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(TabRedirectionService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
