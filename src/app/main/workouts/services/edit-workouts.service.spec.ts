import { TestBed } from '@angular/core/testing';

import { EditWorkoutsService } from './edit-workouts.service';

describe('EditWorkoutsService', () => {
  let service: EditWorkoutsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditWorkoutsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
