import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TabName } from '../models/workout-tab-name.model';

@Injectable({
    providedIn: 'root'
})
export class TabRedirectionService {
    requests: BehaviorSubject<TabName> = new BehaviorSubject(
        'search-workouts' as TabName
    );
}
