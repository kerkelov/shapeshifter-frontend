import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Workout } from '../models/workout.model';

@Injectable({
    providedIn: 'root'
})
export class EditWorkoutsService {
    _workoutsForEditing: BehaviorSubject<Workout[]> = new BehaviorSubject<
        Workout[]
    >([]);

    public readonly workoutsForEditing: Observable<Workout[]> =
        this._workoutsForEditing.asObservable();

    private workoutsForEditingMap: Map<string, Workout> = new Map();

    addWorkoutForEditing(...workouts: Workout[]): void {
        workouts.forEach((workout) => {
            if (!this.workoutsForEditingMap.has(workout._id)) {
                this.workoutsForEditingMap.set(workout._id, workout);
            }
        });

        this._workoutsForEditing.next([...this.workoutsForEditingMap.values()]);
    }

    removeWorkout(...workoutIds: string[]): void {
        workoutIds.forEach((workoutId) =>
            this.workoutsForEditingMap.delete(workoutId)
        );

        this._workoutsForEditing.next([...this.workoutsForEditingMap.values()]);
    }
}
