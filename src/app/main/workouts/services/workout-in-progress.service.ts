import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Workout } from '../models/workout.model';

@Injectable({
    providedIn: 'root'
})
export class WorkoutInProgressService {
    private currentWorkoutKey: string = 'currentWorkout';
    currentWorkout: BehaviorSubject<Workout | null>;

    constructor() {
        this.currentWorkout = new BehaviorSubject(
            JSON.parse(localStorage.getItem(this.currentWorkoutKey) || 'null')
        );
    }

    setCurrentWorkout(workout: Workout): void {
        localStorage.setItem(this.currentWorkoutKey, JSON.stringify(workout));
        this.currentWorkout.next(workout);
    }

    getCurrentWorkout(): Workout | null {
        return this.currentWorkout.getValue();
    }

    clear(): void {
        localStorage.removeItem(this.currentWorkoutKey);
        this.currentWorkout.next(null);
    }
}
