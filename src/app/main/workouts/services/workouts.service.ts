import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of, Subject, switchMap, tap } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { CurrentUserService } from '../../../core/services/current-user.service';
import { formatDate } from '../../../core/utils/format-date';
import { Exercise } from '../../exercises/models/exercise.model';
import { ExerciseExecution } from '../models/exercise-execution.model';
import { SearchWorkoutQuery } from '../models/search-workout-query.model';
import { WorkoutRequest } from '../models/workout-request.model';
import { WorkoutUpdateInfo } from '../models/workout-update-info.model';
import { Workout } from '../models/workout.model';
import { ExerciseExecutionsService } from './exercise-executions.service';

@Injectable({
    providedIn: 'root'
})
export class WorkoutsService {
    workoutsBackendUrl: string = `${environment.backendUrl}/workout-executions/`;
    workoutUpdated: Subject<WorkoutUpdateInfo> = new Subject();
    lastChartExerciseSelected: Exercise | null;

    constructor(
        private httpClient: HttpClient,
        private currentUserService: CurrentUserService,
        private exerciseExecutionsService: ExerciseExecutionsService
    ) {}

    getAll(): Observable<Workout[]> {
        return this.httpClient.get<Workout[]>(this.workoutsBackendUrl);
    }

    create(workout: Workout): Observable<Workout> {
        return of(workout._id).pipe(
            tap(() => {
                if (workout._id) {
                    this.remove(workout).subscribe();
                }
            }),
            switchMap(() => this.getWorkoutRequest(workout)),
            switchMap((workoutRequest) => {
                workoutRequest.user =
                    this.currentUserService.getCurrentUser()!._id;

                workoutRequest.date = new Date(formatDate(workoutRequest.date));

                return this.httpClient.post<Workout>(
                    this.workoutsBackendUrl,
                    workoutRequest
                );
            })
        );
    }

    getById(id: string): Observable<Workout> {
        return this.httpClient.get<Workout>(this.workoutsBackendUrl + id);
    }

    getByDate(date: Date): Observable<Workout[] | null> {
        const theDateInProperFormat = new Date(formatDate(date)).toISOString();

        return this.httpClient
            .get<Workout[]>(
                `${this.workoutsBackendUrl}byDate/` +
                    this.currentUserService.getCurrentUser()?._id,
                {
                    params: {
                        date: theDateInProperFormat
                    }
                }
            )
            .pipe(catchError(() => of(null)));
    }

    getBetweenDates(
        startDate: Date,
        endDate: Date
    ): Observable<Map<string, Workout[]>> {
        return this.httpClient
            .get<[string, Workout[]][]>(
                `${this.workoutsBackendUrl}betweenDates/` +
                    this.currentUserService.getCurrentUser()?._id,
                {
                    params: {
                        startDate: startDate.toISOString(),
                        endDate: endDate.toISOString()
                    }
                }
            )
            .pipe(
                map((tupleMatrix: [string, Workout[]][]) => {
                    const map: Map<string, Workout[]> = new Map();

                    tupleMatrix.forEach((tuple) => {
                        const date: string = tuple[0],
                            workouts: Workout[] = tuple[1];

                        if (!map.has(date)) {
                            map.set(date, []);
                        }

                        map.get(date)?.push(...workouts);
                    });

                    return map;
                })
            );
    }

    searchWorkouts({
        startDate: from,
        endDate: to,
        musclesEngaged,
        exercisesPerformed
    }: SearchWorkoutQuery): Observable<Workout[]> {
        const datesInISOFormat = {
            startDate: from.toISOString(),
            endDate: to.toISOString()
        };

        const query: Omit<SearchWorkoutQuery, 'startDate' | 'endDate'> = {};

        if (musclesEngaged) {
            query.musclesEngaged = musclesEngaged;
        } else if (exercisesPerformed) {
            query.exercisesPerformed = exercisesPerformed;
        }

        return this.httpClient.post<Workout[]>(
            `${this.workoutsBackendUrl}search/` +
                this.currentUserService.getCurrentUser()?._id,
            {
                ...datesInISOFormat,
                ...query
            }
        );
    }

    remove(workout: Workout): Observable<unknown> {
        //TODO: implement hard delete on the backend
        this.exerciseExecutionsService.removeMany(workout.exerciseExecutions);

        return this.httpClient.delete<unknown>(
            this.workoutsBackendUrl + workout._id
        );
    }

    fetchAllWorkoutsForMonth(
        someDateOfThatMonth: Date
    ): Observable<Map<string, Workout[]>> {
        const year = someDateOfThatMonth.getFullYear(),
            month = someDateOfThatMonth.getMonth();

        const firstDayOfTheMonth = new Date(year, month, 1),
            lastDayOfTheMonth = new Date(year, month + 1, 0);

        return this.getBetweenDates(firstDayOfTheMonth, lastDayOfTheMonth);
    }

    private getWorkoutRequest(workout: Workout): Observable<WorkoutRequest> {
        const workoutTemplateId = workout.workoutTemplate?._id;
        delete workout.workoutTemplate;

        this.exerciseExecutionsService.removeMany(workout.exerciseExecutions);

        return this.exerciseExecutionsService
            .createMany(workout.exerciseExecutions)
            .pipe(
                map((exerciseExecutionsResponses: ExerciseExecution[]) => {
                    return {
                        ...workout,
                        exerciseExecutions: exerciseExecutionsResponses.map(
                            (exerciseExecution) => exerciseExecution._id ?? ''
                        ),
                        workoutTemplate: workoutTemplateId
                    };
                })
            );
    }
}
