import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { WorkoutsCalendarHeaderComponent } from '../../components/workouts-calendar-header/workouts-calendar-header.component';
import { TabName } from '../../models/workout-tab-name.model';
import { Workout } from '../../models/workout.model';
import { TabRedirectionService } from '../../services/tab-redirection.service';
import { WorkoutInProgressService } from '../../services/workout-in-progress.service';

@Component({
    selector: 'app-workouts-page',
    templateUrl: './workouts-page.component.html',
    styleUrls: ['./workouts-page.component.scss']
})
export class WorkoutsPageComponent implements OnInit, OnDestroy {
    workoutsCalendar: typeof WorkoutsCalendarHeaderComponent =
        WorkoutsCalendarHeaderComponent;
    selectedTabIndex: number = 1;
    allSubscriptions: Subscription[] = [];

    constructor(
        public workoutInProgressService: WorkoutInProgressService,
        private tabRedirectionService: TabRedirectionService
    ) {}

    ngOnInit(): void {
        this.navigateBetweenTabsWhenRequested();
    }

    ngOnDestroy(): void {
        this.allSubscriptions.forEach((subscription) => {
            subscription.unsubscribe();
        });
    }

    navigateBetweenTabsWhenRequested(): void {
        this.allSubscriptions.push(
            this.tabRedirectionService.requests.subscribe(
                (newTabName: TabName) => this.goToTab(newTabName)
            )
        );
    }

    currentWorkoutClosed(): void {
        this.workoutInProgressService.clear();
    }

    updateTheFormTemplate(workout: Workout): void {
        this.workoutInProgressService.setCurrentWorkout(workout);
    }

    goToTab(name: TabName): void {
        if (name === 'create-workout') {
            this.selectedTabIndex = 0;
        } else if (name === 'workouts-calendar') {
            this.selectedTabIndex = 1;
        } else if (name === 'search-workouts') {
            this.selectedTabIndex = 2;
        }
    }

    selectedIndexChanged(indexSelected: number): void {
        this.selectedTabIndex = indexSelected;
    }
}
