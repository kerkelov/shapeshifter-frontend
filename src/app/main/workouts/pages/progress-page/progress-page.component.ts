import { Component, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LegendPosition } from '@swimlane/ngx-charts';
import { Subscription, take } from 'rxjs';
import { formatDate } from '../../../../core/utils/format-date';
import { ChartData, Serie } from '../../../../shared/models/chart-data.model';
import { DateRange } from '../../../../shared/models/date-range.model';
import { Exercise } from '../../../exercises/models/exercise.model';
import { SearchExercisesDialogComponent } from '../../components/search-exercises-dialog/search-exercises-dialog.component';
import { ExerciseExecution } from '../../models/exercise-execution.model';
import { Workout } from '../../models/workout.model';
import { WorkoutsService } from '../../services/workouts.service';

@Component({
    selector: 'app-progress-page',
    templateUrl: './progress-page.component.html',
    styleUrls: ['./progress-page.component.scss']
})
export class ProgressPageComponent implements OnDestroy {
    dataToVisualize: ChartData[] = [];
    legendPosition: LegendPosition = LegendPosition.Below;
    currentSeries: Serie[] = [];
    currentPeriod: DateRange;
    showChart: boolean = false;
    allSubscriptions: Subscription[] = [];

    constructor(
        public workoutsService: WorkoutsService,
        private snackBar: MatSnackBar,
        private dialog: MatDialog
    ) {}

    ngOnDestroy(): void {
        this.allSubscriptions.forEach((subscription) =>
            subscription.unsubscribe()
        );
    }

    updateTheDataToVisualize(periodData: Workout[]): void {
        if (periodData.length < 2) {
            this.snackBar.open(
                `Not enough data for ${
                    this.workoutsService.lastChartExerciseSelected!.name
                } yet.`,
                'Close',
                {
                    panelClass: 'round-white-background'
                }
            );

            this.workoutsService.lastChartExerciseSelected = null;
            return;
        }

        this.populateTheCurrentSeries(periodData);

        this.dataToVisualize = [
            {
                name: this.workoutsService.lastChartExerciseSelected!.name,
                series: this.currentSeries
            }
        ];
        this.showChart = true;
    }

    populateTheCurrentSeries(periodData: Workout[]): void {
        periodData.forEach((workout) => {
            const name: string = formatDate(new Date(workout.date));

            const executionOfTheCurrentExercise: ExerciseExecution | undefined =
                workout.exerciseExecutions.find((exerciseExecution) =>
                    this.IsExerciseExecutionOfTheCurrentExercise(
                        exerciseExecution
                    )
                );

            const value: number = this.CalculateTheAverageWeightLifted(
                executionOfTheCurrentExercise!
            );
            console.log(value);
            this.currentSeries.push({
                name,
                value
            });
        });
    }

    IsExerciseExecutionOfTheCurrentExercise(
        exerciseExecution: ExerciseExecution
    ): boolean {
        return typeof exerciseExecution.exercise === 'string'
            ? exerciseExecution.exercise ===
                  this.workoutsService.lastChartExerciseSelected!._id
            : exerciseExecution.exercise._id ===
                  this.workoutsService.lastChartExerciseSelected!._id;
    }

    CalculateTheAverageWeightLifted(
        exerciseExecution: ExerciseExecution
    ): number {
        return (
            exerciseExecution.sets.reduce(
                (acc, curr) => acc + curr.kilograms! * curr.reps!,
                0
            ) / exerciseExecution.sets.length
        );
    }

    openSearchExercisesDialog(): void {
        const dialogRef = this.dialog.open(SearchExercisesDialogComponent, {
            data: {
                allowOnlyOneSelection: true
            },
            autoFocus: false,
            width: '70vw',
            panelClass: 'round-without-padding'
        });

        this.handleDialogClosing(dialogRef);
    }

    handleDialogClosing(
        dialog: MatDialogRef<SearchExercisesDialogComponent>
    ): void {
        this.allSubscriptions.push(
            dialog.afterClosed().subscribe((exercisesSelected: Exercise[]) => {
                const exerciseSelected = exercisesSelected[0];
                if (exerciseSelected.isCardio) {
                    this.snackBar.open(
                        `Charts for cardio exercises such as ${exerciseSelected.name} are not supported yet`,
                        'Close',
                        {
                            panelClass: 'round-white-background'
                        }
                    );

                    return;
                }

                this.workoutsService.lastChartExerciseSelected =
                    exerciseSelected;

                if (this.currentPeriod) {
                    this.onRangeSelectionReady(this.currentPeriod);
                }
            })
        );
    }

    onRangeSelectionReady(dateRange: DateRange): void {
        this.currentPeriod = dateRange;

        if (!this.workoutsService.lastChartExerciseSelected) {
            return;
        }

        this.workoutsService
            .searchWorkouts({
                startDate: dateRange.startDate,
                endDate: dateRange.endDate,
                exercisesPerformed: [
                    this.workoutsService.lastChartExerciseSelected
                ]
            })
            .pipe(take(1))
            .subscribe((periodData) =>
                this.updateTheDataToVisualize(periodData)
            );
    }
}
