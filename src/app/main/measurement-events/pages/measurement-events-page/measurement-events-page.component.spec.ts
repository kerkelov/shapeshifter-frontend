import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurementEventsPageComponent } from './measurement-events-page.component';

describe('MeasurementEventsPageComponent', () => {
  let component: MeasurementEventsPageComponent;
  let fixture: ComponentFixture<MeasurementEventsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeasurementEventsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasurementEventsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
