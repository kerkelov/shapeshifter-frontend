import { Component } from '@angular/core';
import { MsCalendarHeaderComponent } from '../../components/ms-calendar-header/ms-calendar-header.component';

@Component({
    selector: 'app-measurement-events-page',
    templateUrl: './measurement-events-page.component.html',
    styleUrls: ['./measurement-events-page.component.scss']
})
export class MeasurementEventsPageComponent {
    header = MsCalendarHeaderComponent;
}
