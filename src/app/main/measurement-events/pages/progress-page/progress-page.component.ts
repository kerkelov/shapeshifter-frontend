import { Component } from '@angular/core';
import { LegendPosition } from '@swimlane/ngx-charts';
import { take } from 'rxjs';
import { formatDate } from '../../../../core/utils/format-date';
import { ChartData, Serie } from '../../../../shared/models/chart-data.model';
import { DateRange } from '../../../../shared/models/date-range.model';
import { MeasurementEvent } from '../../models/measurement-event.model';
import { MeasurementEventsService } from '../../services/measurement-events.service';

@Component({
    selector: 'app-progress-page',
    templateUrl: './progress-page.component.html',
    styleUrls: ['./progress-page.component.scss']
})
export class ProgressPageComponent {
    dataToVisualize: ChartData[] = [];
    legendPosition: LegendPosition = LegendPosition.Below;
    bicepsSeries: Serie[] = [];
    waistSeries: Serie[] = [];
    hipsSeries: Serie[] = [];
    weightSeries: Serie[] = [];
    chestSeries: Serie[] = [];

    constructor(private measurementEventsService: MeasurementEventsService) {}

    addToTheCorrectSeries: (measurement: MeasurementEvent) => void = (
        measurement: MeasurementEvent
    ) => {
        const name = formatDate(new Date(measurement.date));

        this.bicepsSeries.push({
            name,
            value: measurement.biceps
        });

        this.waistSeries.push({
            name,
            value: measurement.waist
        });

        this.hipsSeries.push({
            name,
            value: measurement.hips
        });

        this.weightSeries.push({
            name,
            value: measurement.weight
        });

        this.chestSeries.push({
            name,
            value: measurement.chest
        });
    };

    emptyOutAllTheSeries(): void {
        this.bicepsSeries = [];
        this.waistSeries = [];
        this.hipsSeries = [];
        this.weightSeries = [];
        this.chestSeries = [];
    }

    updateTheDataToVisualize: (periodData: MeasurementEvent[]) => void = (
        periodData: MeasurementEvent[]
    ) => {
        periodData.forEach(this.addToTheCorrectSeries);

        this.dataToVisualize = [
            {
                name: 'waist',
                series: this.waistSeries
            },
            {
                name: 'weight',
                series: this.weightSeries
            },
            {
                name: 'hips',
                series: this.hipsSeries
            },
            {
                name: 'biceps',
                series: this.bicepsSeries
            },
            {
                name: 'chest',
                series: this.chestSeries
            }
        ];

        this.emptyOutAllTheSeries();
    };

    onRangeSelectionReady(dateRange: DateRange): void {
        this.measurementEventsService
            .getBetweenDates(dateRange.startDate, dateRange.endDate)
            .pipe(take(1))
            .subscribe(this.updateTheDataToVisualize);
    }
}
