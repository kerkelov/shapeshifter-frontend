import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../shared/shared.module';
import { MainLayoutModule } from '../main-layout/main-layout.module';
import { CreateMsSectionComponent } from './components/create-ms-section/create-ms-section.component';
import { MsCalendarHeaderComponent } from './components/ms-calendar-header/ms-calendar-header.component';
import { MsDialog } from './components/ms-dialog/ms-dialog.component';
import { MsFormComponent } from './components/ms-form/ms-form.component';
import { MeasurementEventsRoutingModule } from './measurement-events-routing.module';
import { MeasurementEventsPageComponent } from './pages/measurement-events-page/measurement-events-page.component';
import { ProgressPageComponent } from './pages/progress-page/progress-page.component';

@NgModule({
    declarations: [
        ProgressPageComponent,
        MeasurementEventsPageComponent,
        MsDialog,
        MsCalendarHeaderComponent,
        CreateMsSectionComponent,
        MsFormComponent
    ],
    imports: [
        CommonModule,
        MeasurementEventsRoutingModule,
        ReactiveFormsModule,
        SharedModule,
        MatFormFieldModule,
        MatInputModule,
        MainLayoutModule,
        MatSnackBarModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatButtonModule,
        MatCardModule,
        NgxChartsModule
    ]
})
export class MeasurementEventsModule {}
