import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MsCalendarHeaderComponent } from './ms-calendar-header.component';

describe('MsCalendarHeaderComponent', () => {
  let component: MsCalendarHeaderComponent;
  let fixture: ComponentFixture<MsCalendarHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MsCalendarHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MsCalendarHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
