import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { MatCalendar } from '@angular/material/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { delay, Subscription, take } from 'rxjs';
import { formatDate } from '../../../../core/utils/format-date';
import { MeasurementEvent } from '../../models/measurement-event.model';
import { MeasurementEventUpdateInfo } from '../../models/ms-update-info.model';
import { MeasurementEventsService } from '../../services/measurement-events.service';
import { MsDialog } from '../ms-dialog/ms-dialog.component';

@Component({
    selector: 'app-ms-calendar-header',
    templateUrl: './ms-calendar-header.component.html',
    styleUrls: ['./ms-calendar-header.component.scss']
})
export class MsCalendarHeaderComponent implements OnInit, OnDestroy {
    currentMonthData: Map<string, MeasurementEvent> = new Map();
    isShowingSpinner: boolean;
    today: Date = new Date();
    allSubscriptions: Subscription[] = [];

    constructor(
        private calendar: MatCalendar<Date>,
        private dateAdapter: DateAdapter<Date>,
        private measurementEventsService: MeasurementEventsService,
        private snackBar: MatSnackBar,
        private dialog: MatDialog
    ) {}

    get periodLabel(): string {
        const dateString = this.calendar.activeDate.toDateString(),
            dateStringParts = dateString.split(' '),
            month = dateStringParts[1],
            year = dateStringParts[3];

        return `${month} ${year}`;
    }

    ngOnDestroy(): void {
        this.allSubscriptions.forEach(
            (subscription) => subscription.unsubscribe
        );
    }

    ngOnInit(): void {
        this.calendar.dateFilter = (date: Date) =>
            !!this.currentMonthData.get(formatDate(date));

        this.openMsDialogWhenNewDateIsSelected();
        this.getNotifiedWhenMeasurementEventIsUpdated();
        this.refreshWhatIsSelectable();
    }

    refreshWhatIsSelectable(): void {
        const previousActiveDate = this.calendar.activeDate;

        this.calendar._goToDateInView(
            this.dateAdapter.addCalendarYears(this.calendar.activeDate, -50),
            'month'
        );

        this.loadNewMonth(previousActiveDate);
    }

    headerButtonClicked(
        period: 'month' | 'year',
        action: 'next' | 'previous'
    ): void {
        let newActiveDate: Date;
        const amount = action === 'next' ? 1 : -1;

        if (period === 'year') {
            newActiveDate = this.dateAdapter.addCalendarYears(
                this.calendar.activeDate,
                amount
            );
        } else {
            newActiveDate = this.dateAdapter.addCalendarMonths(
                this.calendar.activeDate,
                amount
            );
        }

        this.loadNewMonth(newActiveDate);
    }

    openMsDialogWhenNewDateIsSelected() {
        this.allSubscriptions.push(
            this.calendar.selectedChange.subscribe((newDate: Date) => {
                this.dialog.open(MsDialog, {
                    width: '60vw',
                    autoFocus: false,
                    panelClass: 'round-without-padding',
                    data: this.currentMonthData.get(formatDate(newDate))
                });
            })
        );
    }

    loadNewMonth(someDateOfThatMonth: Date): void {
        this.isShowingSpinner = true;

        const newMonthFetchedObserver = {
            next: (newMonthData: Map<string, MeasurementEvent>) => {
                this.currentMonthData = newMonthData;
                this.calendar.activeDate = someDateOfThatMonth;
                this.isShowingSpinner = false;
            },
            error: (httpError: HttpErrorResponse) => {
                this.snackBar.open(httpError.error.message, 'Close', {
                    panelClass: 'round-white-background'
                });
            }
        };

        this.measurementEventsService
            .fetchAllMeasurementEventsForMonth(someDateOfThatMonth)
            .pipe(take(1))
            .pipe(delay(500))
            .subscribe(newMonthFetchedObserver);
    }

    getNotifiedWhenMeasurementEventIsUpdated(): void {
        this.allSubscriptions.push(
            this.measurementEventsService.msUpdated.subscribe(
                (info: MeasurementEventUpdateInfo) => {
                    const date = formatDate(info.measurementEvent.date),
                        month = parseInt(date.split('-')[0]),
                        year = parseInt(date.split('-')[2]),
                        calendarMonth = this.calendar.activeDate.getMonth() + 1,
                        calendarYear = this.calendar.activeDate.getFullYear();

                    if (month !== calendarMonth || year !== calendarYear) {
                        return;
                    }

                    if (info.action === 'DELETE') {
                        this.currentMonthData.delete(date);
                    } else {
                        this.currentMonthData.set(date, info.measurementEvent);
                    }

                    if (info.action === 'DELETE' || info.action === 'CREATE') {
                        this.refreshWhatIsSelectable();
                    }
                }
            )
        );
    }
}
