import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MsFormComponent } from './ms-form.component';

describe('MsFormComponent', () => {
  let component: MsFormComponent;
  let fixture: ComponentFixture<MsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
