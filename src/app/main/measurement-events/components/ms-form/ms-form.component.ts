import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges
} from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import { Subscription } from 'rxjs';
import { blankProfilePictureUrl } from '../../../../core/constants/urls';
import { formatDate } from '../../../../core/utils/format-date';
import { MeasurementEvent } from '../../models/measurement-event.model';

@Component({
    selector: 'app-ms-form',
    templateUrl: './ms-form.component.html',
    styleUrls: ['./ms-form.component.scss']
})
export class MsFormComponent implements OnInit, OnChanges {
    @Input() measurementEventTemplate: MeasurementEvent | null;
    @Input() canEdit: boolean;

    @Output() onSubmit = new EventEmitter<MeasurementEvent>();
    @Output() onCancel = new EventEmitter();
    @Output() onDelete = new EventEmitter();

    measurementEventsForm: FormGroup;
    allSubscriptions: Subscription[] = [];

    constructor(private formBuilder: FormBuilder) {}

    get pictureControl(): AbstractControl | null {
        return this.measurementEventsForm.get('photosUrls');
    }

    get weightControl(): AbstractControl | null {
        return this.measurementEventsForm.get('weight');
    }

    get chestControl(): AbstractControl | null {
        return this.measurementEventsForm.get('chest');
    }

    get waistControl(): AbstractControl | null {
        return this.measurementEventsForm.get('waist');
    }

    get hipsControl(): AbstractControl | null {
        return this.measurementEventsForm.get('hips');
    }

    get bicepsControl(): AbstractControl | null {
        return this.measurementEventsForm.get('biceps');
    }

    ngOnInit(): void {
        this.setupTheMeasurementEventsForm();
    }

    ngOnChanges(changes: SimpleChanges): void {
        const measurementEventTemplateChanged =
            changes['measurementEventTemplate'];

        if (
            measurementEventTemplateChanged &&
            !measurementEventTemplateChanged.firstChange
        ) {
            this.setupTheMeasurementEventsForm();
        }
    }

    ngOnDestroy(): void {
        this.allSubscriptions.forEach((subscription) =>
            subscription.unsubscribe()
        );
    }

    saveClicked(): void {
        this.onSubmit.emit(this.measurementEventsForm.value);
    }

    cancelClicked(): void {
        this.onCancel.emit();
    }

    deleteClicked(): void {
        this.onDelete.emit();
    }

    newPictureUploaded(newPictureUrl: string) {
        this.pictureControl?.setValue(newPictureUrl);
    }

    setupTheMeasurementEventsForm(): void {
        this.measurementEventsForm = this.formBuilder.group({
            photosUrls: new FormControl(blankProfilePictureUrl),

            weight: new FormControl(this.measurementEventTemplate?.weight, [
                Validators.required,
                Validators.min(10),
                Validators.max(250)
            ]),
            chest: new FormControl(this.measurementEventTemplate?.chest, [
                Validators.required,
                Validators.min(20),
                Validators.max(150)
            ]),
            waist: new FormControl(this.measurementEventTemplate?.waist, [
                Validators.required,
                Validators.min(20),
                Validators.max(150)
            ]),
            hips: new FormControl(this.measurementEventTemplate?.hips, [
                Validators.required,
                Validators.min(20),
                Validators.max(150)
            ]),
            biceps: new FormControl(this.measurementEventTemplate?.biceps, [
                Validators.required,
                Validators.min(10),
                Validators.max(60)
            ]),
            date: new FormControl(
                this.measurementEventTemplate?.date || formatDate(new Date())
            )
        });
    }
}
