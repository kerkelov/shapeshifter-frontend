import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMsSectionComponent } from './create-ms-section.component';

describe('CreateMsSectionComponent', () => {
    let component: CreateMsSectionComponent;
    let fixture: ComponentFixture<CreateMsSectionComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [CreateMsSectionComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CreateMsSectionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
