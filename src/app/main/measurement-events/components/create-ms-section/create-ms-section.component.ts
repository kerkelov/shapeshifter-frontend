import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, take } from 'rxjs';
import { formatDate } from '../../../../core/utils/format-date';
import { MeasurementEvent } from '../../models/measurement-event.model';
import { MeasurementEventUpdateInfo } from '../../models/ms-update-info.model';
import { MeasurementEventsService } from '../../services/measurement-events.service';
import { MsDialog } from '../ms-dialog/ms-dialog.component';

@Component({
    selector: 'app-create-ms-section',
    templateUrl: './create-ms-section.component.html',
    styleUrls: ['./create-ms-section.component.scss']
})
export class CreateMsSectionComponent implements OnInit, OnDestroy {
    measurementEventForToday: MeasurementEvent | null;
    allSubscriptions: Subscription[] = [];

    constructor(
        private measurementEventsService: MeasurementEventsService,
        private dialog: MatDialog
    ) {}

    ngOnInit(): void {
        this.fetchTheMeasurementEventForToday();
        this.getNotifiedWhenTheMeasurementEventForTodayIsUpdated();
    }

    ngOnDestroy(): void {
        this.allSubscriptions.forEach((subscription) =>
            subscription.unsubscribe()
        );
    }

    openMsDialog(): void {
        this.dialog.open(MsDialog, {
            width: '60vw',
            autoFocus: false,
            panelClass: 'round-without-padding'
        });
    }

    fetchTheMeasurementEventForToday(): void {
        this.measurementEventsService
            .getByDate(new Date())
            .pipe(take(1))
            .subscribe(
                (measurementEvent) =>
                    (this.measurementEventForToday = measurementEvent)
            );
    }

    getNotifiedWhenTheMeasurementEventForTodayIsUpdated(): void {
        this.allSubscriptions.push(
            this.measurementEventsService.msUpdated.subscribe(
                (info: MeasurementEventUpdateInfo) => {
                    if (
                        formatDate(new Date()) !==
                        formatDate(info.measurementEvent.date)
                    ) {
                        return;
                    }

                    if (info.action === 'DELETE') {
                        this.measurementEventForToday = null;
                    } else {
                        this.measurementEventForToday = info.measurementEvent;
                    }
                }
            )
        );
    }
}
