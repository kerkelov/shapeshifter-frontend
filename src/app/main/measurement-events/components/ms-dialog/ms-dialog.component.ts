import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs';
import { MeasurementEvent } from '../../models/measurement-event.model';
import { MeasurementEventsService } from '../../services/measurement-events.service';

@Component({
    selector: 'app-ms-dialog',
    templateUrl: './ms-dialog.component.html',
    styleUrls: ['./ms-dialog.component.scss']
})
export class MsDialog implements OnInit {
    canEdit: boolean;
    deleteButtonVisible: boolean;
    today: Date = new Date();

    constructor(
        @Inject(MAT_DIALOG_DATA)
        public measurementEventTemplate: MeasurementEvent | null,
        private dialogRef: MatDialogRef<MsDialog>,
        private measurementEventsService: MeasurementEventsService,
        private snackBar: MatSnackBar
    ) {}

    ngOnInit(): void {
        this.canEdit = !this.measurementEventTemplate;
    }

    editIconClicked(): void {
        this.canEdit = true;
    }

    deleteClicked(): void {
        const observer = {
            next: () => {
                this.closeClicked();
                this.snackBar.open('Your deletion was successfull', 'Close', {
                    panelClass: 'round-white-background'
                });
                this.measurementEventsService.msUpdated.next({
                    measurementEvent: this.measurementEventTemplate!,
                    action: 'DELETE'
                });
            },
            error: (httpError: HttpErrorResponse) => {
                this.snackBar.open(httpError.error.measurement, 'Close', {
                    panelClass: 'round-white-background'
                });
            }
        };

        this.measurementEventsService
            .remove(this.measurementEventTemplate!._id)
            .pipe(take(1))
            .subscribe(observer);
    }

    cancelClicked(): void {
        if (!this.measurementEventTemplate) {
            this.closeClicked();
        } else {
            this.measurementEventTemplate = Object.assign(
                {},
                this.measurementEventTemplate
            );

            this.canEdit = false;
        }
    }

    closeClicked(): void {
        this.dialogRef.close();
    }

    submitted(measurementEvent: MeasurementEvent): void {
        if (!Array.isArray(measurementEvent.photosUrls)) {
            measurementEvent.photosUrls = [measurementEvent.photosUrls];
        }

        const observer = {
            next: (measurementEvent: MeasurementEvent) => {
                this.snackBar.open(
                    this.measurementEventTemplate
                        ? 'Your change was successfull'
                        : 'Your creation was successfull',
                    'Close',
                    { panelClass: 'round-white-background' }
                );

                this.closeClicked();

                this.measurementEventsService.msUpdated.next({
                    measurementEvent,
                    action: this.measurementEventTemplate ? 'UPDATE' : 'CREATE'
                });
            },
            error: (httpError: HttpErrorResponse) => {
                this.snackBar.open(httpError.error.measurement, 'Close', {
                    panelClass: 'round-white-background'
                });
            }
        };

        let observable;
        if (this.measurementEventTemplate) {
            observable = this.measurementEventsService.update(
                this.measurementEventTemplate._id,
                measurementEvent
            );
        } else {
            observable = this.measurementEventsService.create(measurementEvent);
        }

        observable.pipe(take(1)).subscribe(observer);
    }
}
