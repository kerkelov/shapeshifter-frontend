import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurementEventComponent } from './ms-dialog.component';

describe('MsDialogComponent', () => {
    let component: MeasurementEventComponent;
    let fixture: ComponentFixture<MeasurementEventComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [MeasurementEventComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MeasurementEventComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
