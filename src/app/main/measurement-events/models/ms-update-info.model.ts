import { MeasurementEvent } from './measurement-event.model';

export type MeasurementEventUpdateInfo = {
    measurementEvent: MeasurementEvent;
    action?: 'UPDATE' | 'DELETE' | 'CREATE';
};
