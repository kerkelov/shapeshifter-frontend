export type MeasurementEvent = {
    _id: string;
    photosUrls: string[];
    weight: number;
    chest: number;
    waist: number;
    hips: number;
    biceps: number;
    date: Date;
    user: string;
};
