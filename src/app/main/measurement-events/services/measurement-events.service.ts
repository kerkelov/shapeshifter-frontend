import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of, Subject } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { CurrentUserService } from '../../../core/services/current-user.service';
import { formatDate } from '../../../core/utils/format-date';
import { MeasurementEvent } from '../models/measurement-event.model';
import { MeasurementEventUpdateInfo } from '../models/ms-update-info.model';

@Injectable({
    providedIn: 'root'
})
export class MeasurementEventsService {
    measurementEventsBackendUrl: string = `${environment.backendUrl}/measurement-events/`;
    msUpdated: Subject<MeasurementEventUpdateInfo> = new Subject();

    constructor(
        private httpClient: HttpClient,
        private currentUserService: CurrentUserService
    ) {}

    create(measurementEvent: MeasurementEvent): Observable<MeasurementEvent> {
        measurementEvent.user =
            this.currentUserService.getCurrentUser()?._id ?? '';

        return this.httpClient.post<MeasurementEvent>(
            this.measurementEventsBackendUrl,
            measurementEvent
        );
    }

    getById(id: string): Observable<MeasurementEvent> {
        return this.httpClient.get<MeasurementEvent>(
            this.measurementEventsBackendUrl + id
        );
    }

    getByDate(date: Date): Observable<MeasurementEvent | null> {
        const theDateInProperFormat = new Date(formatDate(date)).toISOString();

        return this.httpClient
            .get<MeasurementEvent>(
                `${this.measurementEventsBackendUrl}byDate/` +
                    this.currentUserService.getCurrentUser()?._id,
                {
                    params: {
                        date: theDateInProperFormat
                    }
                }
            )
            .pipe(catchError(() => of(null)));
    }

    getBetweenDates(
        startDate: Date,
        endDate: Date
    ): Observable<MeasurementEvent[]> {
        return this.httpClient.get<MeasurementEvent[]>(
            `${this.measurementEventsBackendUrl}betweenDates/` +
                this.currentUserService.getCurrentUser()?._id,
            {
                params: {
                    startDate: startDate.toISOString(),
                    endDate: endDate.toISOString()
                }
            }
        );
    }

    update(
        id: string,
        partsToUpdate: Partial<MeasurementEvent>
    ): Observable<MeasurementEvent> {
        return this.httpClient.patch<MeasurementEvent>(
            this.measurementEventsBackendUrl + id,
            partsToUpdate
        );
    }

    remove(id: string): Observable<unknown> {
        return this.httpClient.delete<unknown>(
            this.measurementEventsBackendUrl + id
        );
    }

    fetchAllMeasurementEventsForMonth(
        someDateOfThatMonth: Date
    ): Observable<Map<string, MeasurementEvent>> {
        const year = someDateOfThatMonth.getFullYear(),
            month = someDateOfThatMonth.getMonth();

        const firstDayOfTheMonth = new Date(year, month, 1),
            lastDayOfTheMonth = new Date(year, month + 1, 0);

        return this.getBetweenDates(firstDayOfTheMonth, lastDayOfTheMonth).pipe(
            map((measurementEvents: MeasurementEvent[]) => {
                const newMonthData = new Map<string, MeasurementEvent>();

                measurementEvents.forEach((measurementEvent) => {
                    measurementEvent.date = new Date(measurementEvent.date);

                    newMonthData.set(
                        formatDate(measurementEvent.date),
                        measurementEvent
                    );
                });

                return newMonthData;
            })
        );
    }
}
