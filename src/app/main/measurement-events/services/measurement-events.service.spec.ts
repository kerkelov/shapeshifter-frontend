import { TestBed } from '@angular/core/testing';

import { MeasurementEventsService } from './measurement-events.service';

describe('MeasurementEventsService', () => {
  let service: MeasurementEventsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MeasurementEventsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
