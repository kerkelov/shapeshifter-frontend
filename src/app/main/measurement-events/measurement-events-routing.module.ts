import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MeasurementEventsPageComponent } from './pages/measurement-events-page/measurement-events-page.component';
import { ProgressPageComponent } from './pages/progress-page/progress-page.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: MeasurementEventsPageComponent
    },
    {
        path: 'progress',
        component: ProgressPageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MeasurementEventsRoutingModule {}
