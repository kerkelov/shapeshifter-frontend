import { Component } from '@angular/core';

@Component({
    selector: 'app-exercises-page',
    templateUrl: './exercises-page.component.html',
    styleUrls: ['./exercises-page.component.scss']
})
export class ExercisesPageComponent {
    selectedTabIndex: number = 0;

    goToTheEditTab(): void {
        this.selectedTabIndex = 2;
    }

    selectedIndexChanged(indexSelected: number): void {
        this.selectedTabIndex = indexSelected;
    }
}
