import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { SharedModule } from '../../shared/shared.module';
import { MainLayoutModule } from '../main-layout/main-layout.module';
import { ExercisesEditListComponent } from './components/exercises-edit-list/exercises-edit-list.component';
import { ExercisesFormComponent } from './components/exercises-form/exercises-form.component';
import { SearchExercisesCardComponent } from './components/search-exercises-card/search-exercises-card.component';
import { ExercisesRoutingModule } from './exercises-routing.module';
import { ExercisesPageComponent } from './pages/exercises-page/exercises-page.component';

@NgModule({
    declarations: [
        ExercisesPageComponent,
        ExercisesFormComponent,
        SearchExercisesCardComponent,
        ExercisesEditListComponent
    ],
    imports: [
        CommonModule,
        ExercisesRoutingModule,
        MainLayoutModule,
        MatChipsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatInputModule,
        MatAutocompleteModule,
        MatIconModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatTabsModule,
        MatCardModule,
        MatListModule,
        SharedModule
    ]
})
export class ExercisesModule {}
