import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExercisesEditListComponent } from './exercises-edit-list.component';

describe('ExercisesEditListComponent', () => {
  let component: ExercisesEditListComponent;
  let fixture: ComponentFixture<ExercisesEditListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExercisesEditListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExercisesEditListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
