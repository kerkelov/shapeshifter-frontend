import { Component } from '@angular/core';
import { EditExercisesService } from '../../services/edit-exercises.service';

@Component({
    selector: 'app-exercises-edit-list',
    templateUrl: './exercises-edit-list.component.html',
    styleUrls: ['./exercises-edit-list.component.scss']
})
export class ExercisesEditListComponent {
    constructor(public editExercisesService: EditExercisesService) {}

    removeClicked(id: string): void {
        this.editExercisesService.removeExercises(id);
    }
}
