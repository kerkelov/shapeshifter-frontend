import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs';
import { IsNotEmptyArrayValidator } from '../../../../core/utils/validation/is-not-empty-array.validator';
import { Exercise } from '../../models/exercise.model';
import { ExercisesService } from '../../services/exercises.service';

@Component({
    selector: 'app-exercises-form',
    templateUrl: './exercises-form.component.html',
    styleUrls: ['./exercises-form.component.scss']
})
export class ExercisesFormComponent implements OnInit {
    @Input()
    exerciseFormTemplate: Exercise | null;
    @Output()
    close = new EventEmitter<string>();
    createExerciseForm: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private snackBar: MatSnackBar,
        private exercisesService: ExercisesService
    ) {}

    get nameControl(): AbstractControl | null {
        return this.createExerciseForm.get('name');
    }

    get isCardioControl(): AbstractControl | null {
        return this.createExerciseForm.get('isCardio');
    }

    get primaryMuscleControl(): AbstractControl | null {
        return this.createExerciseForm.get('primaryMuscleGroup');
    }

    get secondaryMusclesControl(): AbstractControl | null {
        return this.createExerciseForm.get('secondaryMuscleGroups');
    }

    ngOnInit(): void {
        this.setupTheExercisesForm();
    }

    setupTheExercisesForm(): void {
        this.createExerciseForm = this.formBuilder.group({
            name: new FormControl(this.exerciseFormTemplate?.name, [
                Validators.required
            ]),
            primaryMuscleGroup: new FormControl(
                this.exerciseFormTemplate?.primaryMuscleGroup
                    ? [this.exerciseFormTemplate?.primaryMuscleGroup]
                    : null,
                [IsNotEmptyArrayValidator()]
            ),
            secondaryMuscleGroups: new FormControl(
                this.exerciseFormTemplate?.secondaryMuscleGroups,
                [IsNotEmptyArrayValidator()]
            ),
            isCardio: new FormControl(
                this.exerciseFormTemplate?.isCardio ?? false
            )
        });

        this.primaryMuscleControl?.markAsTouched();
        this.secondaryMusclesControl?.markAsTouched();
    }

    checkboxClicked(value: MatCheckboxChange): void {
        if (value.checked) {
            this.primaryMuscleControl?.setValue(['legs']);
            this.secondaryMusclesControl?.setValue(['core']);
        } else {
            this.primaryMuscleControl?.setValue([]);
            this.secondaryMusclesControl?.setValue([]);
        }
    }

    saveClicked(): void {
        const exercise: Exercise = this.createExerciseForm.value;
        exercise.primaryMuscleGroup = this.primaryMuscleControl?.value[0];

        const observable = this.exerciseFormTemplate
            ? this.exercisesService.update(
                  this.exerciseFormTemplate._id,
                  exercise
              )
            : this.exercisesService.create(exercise);

        const observer = {
            next: (exerciseResponse: Exercise) => {
                this.snackBar.open(
                    `Your ${
                        this.exerciseFormTemplate ? 'edit' : 'creation'
                    } was successful`,
                    'close',
                    { panelClass: 'round-white-background' }
                );

                if (this.exerciseFormTemplate) {
                    this.exerciseFormTemplate = exerciseResponse;
                } else {
                    this.primaryMuscleControl?.setValue([]);
                    this.secondaryMusclesControl?.setValue([]);

                    this.nameControl?.setValue('');
                    this.createExerciseForm.markAsPristine();
                    this.createExerciseForm.markAsUntouched();
                    this.createExerciseForm.updateValueAndValidity();
                }
            },
            error: (httpError: HttpErrorResponse) => {
                this.snackBar.open(httpError.error.message, 'close', {
                    panelClass: 'round-white-background'
                });
            }
        };

        observable.pipe(take(1)).subscribe(observer);
    }

    deleteClicked(): void {
        this.exercisesService
            .remove(this.exerciseFormTemplate?._id ?? '')
            .pipe(take(1))
            .subscribe();

        this.closeClicked();
    }

    closeClicked(): void {
        this.close.emit(this.exerciseFormTemplate?._id);
    }
}
