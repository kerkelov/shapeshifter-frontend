import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { forkJoin, Observable } from 'rxjs';
import { Exercise } from '../../models/exercise.model';
import { EditExercisesService } from '../../services/edit-exercises.service';
import { ExercisesService } from '../../services/exercises.service';

@Component({
    selector: 'app-search-exercises-card',
    templateUrl: './search-exercises-card.component.html',
    styleUrls: ['./search-exercises-card.component.scss']
})
export class SearchExercisesCardComponent {
    exercisesSelected: Exercise[] = [];
    shouldShowSearch: boolean = true;
    @Output() redirectToEditTab = new EventEmitter();

    constructor(
        private exercisesService: ExercisesService,
        private editExercisesService: EditExercisesService,
        private snackBar: MatSnackBar
    ) {}

    onExercisesSelected(exercisesSelected: Exercise[]): void {
        this.exercisesSelected = exercisesSelected;
        this.shouldShowSearch = false;
    }

    showSearch(): void {
        this.exercisesSelected = [];
        this.shouldShowSearch = true;
    }

    editClicked(): void {
        this.editExercisesService.addExercisesForEditing(
            ...this.exercisesSelected
        );

        this.showSearch();
        this.redirectToEditTab.emit();
    }

    deleteClicked(): void {
        const allObservables: Observable<unknown>[] = [],
            observer = {
                next: () => {
                    this.showSearch();
                    this.snackBar.open('Deletion successful', 'Close', {
                        panelClass: 'round-white-background'
                    });
                },
                error: (httpError: HttpErrorResponse) =>
                    this.snackBar.open(httpError.error.message, 'Close', {
                        panelClass: 'round-white-background'
                    })
            };

        this.exercisesSelected.forEach((exercise) =>
            allObservables.push(this.exercisesService.remove(exercise._id))
        );

        forkJoin(allObservables).subscribe(observer);
    }
}
