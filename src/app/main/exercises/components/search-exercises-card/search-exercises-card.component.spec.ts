import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchExercisesCardComponent } from './search-exercises-card.component';

describe('SearchExercisesCardComponent', () => {
    let component: SearchExercisesCardComponent;
    let fixture: ComponentFixture<SearchExercisesCardComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [SearchExercisesCardComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(SearchExercisesCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
