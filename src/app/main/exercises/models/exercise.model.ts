export type Exercise = {
    _id: string;
    name: string;
    primaryMuscleGroup: string;
    secondaryMuscleGroups: string[];
    isCardio: boolean;
};
