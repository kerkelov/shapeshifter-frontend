export const allMuscleGroups: string[] = [
    'traps',
    'shoulders',
    'triceps',
    'biceps',
    'chest',
    'back',
    'forearms',
    'core',
    'calf',
    'legs',
    'neck'
];
