import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Exercise } from '../models/exercise.model';

@Injectable({
    providedIn: 'root'
})
export class ExercisesService {
    exercisesBackendUrl: string = `${environment.backendUrl}/exercises/`;

    constructor(private httpClient: HttpClient) {}

    create(exercise: Exercise): Observable<Exercise> {
        return this.httpClient.post<Exercise>(
            this.exercisesBackendUrl,
            exercise
        );
    }

    getById(id: string): Observable<Exercise> {
        return this.httpClient.get<Exercise>(this.exercisesBackendUrl + id);
    }

    update(id: string, partsToUpdate: Partial<Exercise>): Observable<Exercise> {
        return this.httpClient.patch<Exercise>(
            this.exercisesBackendUrl + id,
            partsToUpdate
        );
    }

    remove(id: string): Observable<unknown> {
        return this.httpClient.delete<unknown>(this.exercisesBackendUrl + id);
    }
}
