import { TestBed } from '@angular/core/testing';

import { EditExercisesService } from './edit-exercises.service';

describe('EditExercisesService', () => {
  let service: EditExercisesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditExercisesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
