import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Exercise } from '../models/exercise.model';

@Injectable({
    providedIn: 'root'
})
export class EditExercisesService {
    _exercisesForEditing: BehaviorSubject<Exercise[]> = new BehaviorSubject<
        Exercise[]
    >([]);

    public readonly exercisesForEditing: Observable<Exercise[]> =
        this._exercisesForEditing.asObservable();

    private exercisesForEditingMap: Map<string, Exercise> = new Map();

    addExercisesForEditing(...exercises: Exercise[]): void {
        exercises.forEach((exercise) => {
            if (!this.exercisesForEditingMap.has(exercise._id)) {
                this.exercisesForEditingMap.set(exercise._id, exercise);
            }
        });

        this._exercisesForEditing.next([
            ...this.exercisesForEditingMap.values()
        ]);
    }

    removeExercises(...exerciseIds: string[]): void {
        exerciseIds.forEach((exerciseId) =>
            this.exercisesForEditingMap.delete(exerciseId)
        );

        this._exercisesForEditing.next([
            ...this.exercisesForEditingMap.values()
        ]);
    }
}
