import { Injectable } from '@angular/core';
import {
    CanActivate,
    CanActivateChild,
    Router,
    UrlTree
} from '@angular/router';
import { CurrentUserService } from '../services/current-user.service';

@Injectable({
    providedIn: 'root'
})
export class IsLoggedInGuard implements CanActivate, CanActivateChild {
    constructor(
        private currentUserService: CurrentUserService,
        private router: Router
    ) {}

    canActivate(): UrlTree | boolean {
        const currentUser = this.currentUserService.getCurrentUser();

        if (currentUser !== null) {
            return true;
        }

        return this.router.parseUrl('public/access-denied');
    }

    canActivateChild(): UrlTree | boolean {
        return this.canActivate();
    }
}
