import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { CurrentUserService } from '../services/current-user.service';

@Injectable({
    providedIn: 'root'
})
export class IsNotLoggedInGuard implements CanActivate {
    constructor(
        private currentUserService: CurrentUserService,
        private router: Router
    ) {}

    canActivate(): UrlTree | boolean {
        const currentUser = this.currentUserService.getCurrentUser();

        if (currentUser === null) {
            return true;
        }

        return this.router.parseUrl('main');
    }
}
