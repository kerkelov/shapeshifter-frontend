import { Injectable } from '@angular/core';
import {
    CanActivate,
    CanActivateChild,
    Router,
    UrlTree
} from '@angular/router';
import { CurrentUserService } from '../services/current-user.service';

@Injectable({
    providedIn: 'root'
})
export class IsAdminGuard implements CanActivate, CanActivateChild {
    constructor(
        private currentUserService: CurrentUserService,
        private router: Router
    ) {}

    canActivate(): boolean | UrlTree {
        if (this.currentUserService.isTheCurrentUserAdmin()) {
            return true;
        }

        return this.router.parseUrl('public/access-denied');
    }

    canActivateChild(): boolean | UrlTree {
        return this.canActivate();
    }
}
