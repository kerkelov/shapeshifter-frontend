export type RegisterUser = {
    username: string;
    email: string;
    password: string;
    roles: string[];
    gender: string;
    height: number;
    dateOfBirth: Date;
    profilePictureUrl: string;
};
