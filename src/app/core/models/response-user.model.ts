import { RegisterUser } from './register-user.model';

export type ResponseUser = Omit<RegisterUser, 'password'> & {
    _id: string;
    roles: string[];
};
