import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';

@Injectable()
export class ConvertDatesFromStringToDateInterceptor
    implements HttpInterceptor
{
    constructor() {}

    intercept(
        request: HttpRequest<unknown>,
        next: HttpHandler
    ): Observable<HttpEvent<unknown>> {
        return next.handle(request).pipe(
            map((httpEvent: HttpEvent<any>) => {
                const response = httpEvent as HttpResponse<any>;

                if (response?.body?.date) {
                    response.body.date = new Date(response.body.date);
                }

                return response;
            })
        );
    }
}
