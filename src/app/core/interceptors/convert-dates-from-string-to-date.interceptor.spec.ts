import { TestBed } from '@angular/core/testing';

import { ConvertDatesFromStringToDateInterceptor } from './convert-dates-from-string-to-date.interceptor';

describe('ConvertDatesFromStringToDateInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ConvertDatesFromStringToDateInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: ConvertDatesFromStringToDateInterceptor = TestBed.inject(ConvertDatesFromStringToDateInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
