import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { LoginUser } from '../models/login-user.model';
import { RegisterUser } from '../models/register-user.model';
import { ResponseUser } from '../models/response-user.model';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    registerBackendUrl: string = `${environment.backendUrl}/users`;
    loginBackendUrl: string = `${environment.backendUrl}/auth/login`;
    logoutBackendUrl: string = `${environment.backendUrl}/auth/logout`;

    constructor(private httpClient: HttpClient) {}

    register(user: RegisterUser): Observable<ResponseUser> {
        return this.httpClient.post<ResponseUser>(
            this.registerBackendUrl,
            user
        );
    }

    login(user: LoginUser): Observable<ResponseUser> {
        return this.httpClient.post<ResponseUser>(this.loginBackendUrl, user);
    }

    logout(): Observable<void> {
        return this.httpClient.post<void>(this.logoutBackendUrl, {});
    }
}
