import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Role } from '../enums/role';
import { ResponseUser } from '../models/response-user.model';

@Injectable({
    providedIn: 'root'
})
export class CurrentUserService {
    private currentUserKey: string = 'currentUser';
    currentUser: BehaviorSubject<ResponseUser | null>;

    constructor() {
        this.currentUser = new BehaviorSubject(
            JSON.parse(localStorage.getItem(this.currentUserKey) ?? 'null')
        );
    }

    setCurrentUser(user: ResponseUser): void {
        this.currentUser.next(user);
        localStorage.setItem(this.currentUserKey, JSON.stringify(user));
    }

    getCurrentUser(): ResponseUser | null {
        return this.currentUser.getValue();
    }

    isTheCurrentUserAdmin(): boolean {
        return this.currentUser.getValue()?.roles.includes(Role.Admin) ?? false;
    }

    clearCurrentUser(): void {
        this.currentUser.next(null);
        //TODO: delete the cookie here
        localStorage.removeItem(this.currentUserKey);
    }
}
