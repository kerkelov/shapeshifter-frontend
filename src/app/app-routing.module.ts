import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsLoggedInGuard } from './core/guards/is-logged-in.guard';
import { NotFoundPageComponent } from './shared/pages/not-found-page/not-found-page.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'public',
        pathMatch: 'full'
    },
    {
        path: 'public',
        loadChildren: () =>
            import('./public/public.module').then((m) => m.PublicModule)
    },
    {
        path: 'main',
        loadChildren: () =>
            import('./main/main.module').then((m) => m.MainModule),
        canActivate: [IsLoggedInGuard],
        canActivateChild: [IsLoggedInGuard]
    },
    {
        path: '**',
        component: NotFoundPageComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
